package by.bsuir.ief.system.languagekourse.controller.table;

import by.bsuir.ief.system.languagekourse.model.entity.TimeTableEntity;
import by.bsuir.ief.system.languagekourse.util.DateUtil;
import by.bsuir.ief.system.languagekourse.util.ListUtil;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.List;

public class TimeTableTableController implements TableController<TimeTableEntity> {

    @FXML
    private AnchorPane rootView;
    @FXML
    private TableView<TimeTableEntity> clientEntityTableView;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private TableColumn<TimeTableEntity, Integer> room;
    @FXML
    private TableColumn<TimeTableEntity, String> dateTime;

    private Stage primaryStage;
    private boolean isMultiSelect;

    private ObservableList<TimeTableEntity> entityObservableList = FXCollections.observableArrayList();
    private CallbackSelected<TimeTableEntity> callbackSelected = null;

    @Override
    public TimeTableEntity getSelect() {
        return null;
    }

    @Override
    public List<TimeTableEntity> getSelected() {
        return null;
    }

    @Override
    public void setList(List<TimeTableEntity> list) {

        entityObservableList = ListUtil.parseListToObserv(entityObservableList, list);

        initTable();

        closeIndicatorProgress();
    }

    @Override
    public void setMultiSelect(boolean multiSelect) {

        isMultiSelect = multiSelect;

        initTable();
    }

    @Override
    public void setCallbackSelect(CallbackSelected<TimeTableEntity> callbackSelect) {
        this.callbackSelected = callbackSelect;
    }

    @Override
    public void initialize() {

        /*rootView.setBackground(Style.getBackground());
        clientEntityTableView.setBackground(Style.getBackground());*/
    }

    @Override
    public void displayIndicatorProgress() {
        clientEntityTableView.setVisible(false);
        progressIndicator.setVisible(true);
    }

    @Override
    public void closeIndicatorProgress() {
        clientEntityTableView.setVisible(true);
        progressIndicator.setVisible(false);
    }

    @Override
    public Pane getLayout() {
        return rootView;
    }

    @Override
    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    private void initTable(){

        clientEntityTableView.getItems().clear();

        room.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getRoom()));
        dateTime.setCellValueFactory(cellData -> new SimpleStringProperty(DateUtil.formatDateTime(cellData.getValue().getDateTime())));

        clientEntityTableView.getSelectionModel().selectedItemProperty().
                addListener(((observable, oldValue, newValue) -> {
                    if(callbackSelected != null) callbackSelected.onSelectItem(newValue);
                }));

        if(isMultiSelect)
            clientEntityTableView.getSelectionModel().setSelectionMode(
                    SelectionMode.MULTIPLE
            );

        clientEntityTableView.setItems(entityObservableList);
    }
}
