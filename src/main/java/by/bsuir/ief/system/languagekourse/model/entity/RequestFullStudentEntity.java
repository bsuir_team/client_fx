package by.bsuir.ief.system.languagekourse.model.entity;

import java.util.Objects;

public class RequestFullStudentEntity {

    private int idRequestStudent;
    private StudentEntity studentEntity;
    private CourseEntity courseEntity;
    private int isSuccessfullyRequest;

    public int getIdRequestStudent() {
        return idRequestStudent;
    }

    public void setIdRequestStudent(int idRequestStudent) {
        this.idRequestStudent = idRequestStudent;
    }

    public StudentEntity getStudentEntity() {
        return studentEntity;
    }

    public void setStudentEntity(StudentEntity studentEntity) {
        this.studentEntity = studentEntity;
    }

    public CourseEntity getCourseEntity() {
        return courseEntity;
    }

    public void setCourseEntity(CourseEntity courseEntity) {
        this.courseEntity = courseEntity;
    }

    public int getIsSuccessfullyRequest() {
        return isSuccessfullyRequest;
    }

    public void setIsSuccessfullyRequest(int isSuccessfullyRequest) {
        this.isSuccessfullyRequest = isSuccessfullyRequest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RequestFullStudentEntity that = (RequestFullStudentEntity) o;
        return idRequestStudent == that.idRequestStudent &&
                isSuccessfullyRequest == that.isSuccessfullyRequest &&
                Objects.equals(studentEntity, that.studentEntity) &&
                Objects.equals(courseEntity, that.courseEntity);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idRequestStudent, studentEntity, courseEntity, isSuccessfullyRequest);
    }
}
