package by.bsuir.ief.system.languagekourse.controller.table;

import by.bsuir.ief.system.languagekourse.model.entity.TeacherEntity;
import by.bsuir.ief.system.languagekourse.util.ListUtil;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.List;

public class TeacherTableController implements TableController<TeacherEntity> {

    @FXML
    private AnchorPane rootView;
    @FXML
    private TableView<TeacherEntity> clientEntityTableView;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private TableColumn<TeacherEntity, String> surname;
    @FXML
    private TableColumn<TeacherEntity, String> name;
    @FXML
    private TableColumn<TeacherEntity, String> patronymic;
    @FXML
    private TableColumn<TeacherEntity, String> language;

    private Stage primaryStage;
    private boolean isMultiSelect;

    private ObservableList<TeacherEntity> entityObservableList = FXCollections.observableArrayList();
    private CallbackSelected<TeacherEntity> callbackSelected = null;

    @Override
    public TeacherEntity getSelect() {
        return clientEntityTableView.getSelectionModel().getSelectedItem();
    }

    @Override
    public List<TeacherEntity> getSelected() {
        return clientEntityTableView.getSelectionModel().getSelectedItems();
    }

    @Override
    public void setList(List<TeacherEntity> list) {

        entityObservableList = ListUtil.parseListToObserv(entityObservableList, list);

        initTable();

        closeIndicatorProgress();

    }

    @Override
    public void setMultiSelect(boolean multiSelect) {

        isMultiSelect = multiSelect;

        initTable();
    }

    @Override
    public void setCallbackSelect(CallbackSelected<TeacherEntity> callbackSelect) {
        this.callbackSelected = callbackSelect;
    }

    @Override
    public void initialize() {

        /*rootView.setBackground(Style.getBackground());
        clientEntityTableView.setBackground(Style.getBackground());*/
    }

    @Override
    public void displayIndicatorProgress() {
        clientEntityTableView.setVisible(false);
        progressIndicator.setVisible(true);
    }

    @Override
    public void closeIndicatorProgress() {
        clientEntityTableView.setVisible(true);
        progressIndicator.setVisible(false);
    }

    @Override
    public Pane getLayout() {
        return rootView;
    }

    @Override
    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    private void initTable(){

        clientEntityTableView.getItems().clear();

        surname.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSurname()));
        name.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        patronymic.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPatronymic()));

        language.setCellValueFactory(cellData -> new SimpleStringProperty((cellData.getValue().getLanguageByIdLanguage().getName())));

        clientEntityTableView.getSelectionModel().selectedItemProperty().
                addListener(((observable, oldValue, newValue) -> {
                    if(callbackSelected != null) callbackSelected.onSelectItem(newValue);
                }));

        if(isMultiSelect)
            clientEntityTableView.getSelectionModel().setSelectionMode(
                    SelectionMode.MULTIPLE
            );

        clientEntityTableView.setItems(entityObservableList);
    }
}
