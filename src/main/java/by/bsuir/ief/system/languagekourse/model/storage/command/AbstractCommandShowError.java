package by.bsuir.ief.system.languagekourse.model.storage.command;

import by.bsuir.ief.system.languagekourse.controller.DialogManager;

public abstract class AbstractCommandShowError<T> extends AbstractObserver<T> {

    @Override
    public void onError(Throwable throwable) {
        DialogManager.showErrorDialog("Error", "Произошла ошибка " + throwable.getMessage());
    }
}
