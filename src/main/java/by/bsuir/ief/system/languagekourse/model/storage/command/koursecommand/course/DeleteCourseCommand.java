package by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.course;


import by.bsuir.ief.system.languagekourse.model.storage.ConnectToServer;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommand;
import connection.client.Request;
import connection.client.requestbody.StringRequestBody;
import io.reactivex.Observable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;

public class DeleteCourseCommand extends AbstractCommand<Boolean> {

    private final int id;

    public DeleteCourseCommand(int id) {
        this.id = id;
    }

    @Override
    protected Request getRequest() throws Exception {
        Request.Builder builder = new Request.Builder()
                .body(new StringRequestBody(String.valueOf(id)))
                .addMethod("deleteByIdCourse");

        builder = ConnectToServer.setConnectData(builder);

        return builder.build();
    }

    @Override
    public Observable<Boolean> execute() {
        return super.getObservable()
                .map(response -> response.isSuccessfully())
                .observeOn(JavaFxScheduler.platform());
    }
}
