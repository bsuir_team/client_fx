package by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.student;

import by.bsuir.ief.system.languagekourse.model.entity.StudentEntity;
import by.bsuir.ief.system.languagekourse.model.storage.ConnectToServer;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommand;
import com.google.gson.GsonBuilder;
import connection.client.Request;
import connection.client.requestbody.StringRequestBody;
import io.reactivex.Observable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;

public class ReadListStudentByIdCommand extends AbstractCommand<StudentEntity> {
    private final int id;

    public ReadListStudentByIdCommand(int id) {
        this.id = id;
    }

    @Override
    protected Request getRequest() throws Exception {
        Request.Builder builder = new Request.Builder()
                .body(new StringRequestBody(String.valueOf(id)))
                .addMethod("readByIdStudent");

        builder = ConnectToServer.setConnectData(builder);

        return builder.build();
    }

    @Override
    public Observable<StudentEntity> execute() {
        return super.getObservable()
                .map(response -> new GsonBuilder().setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz").create().fromJson(new String(response.getBody().bytes()), StudentEntity.class))
                .observeOn(JavaFxScheduler.platform());
    }
}
