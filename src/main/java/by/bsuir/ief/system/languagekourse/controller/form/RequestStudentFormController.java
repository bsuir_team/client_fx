package by.bsuir.ief.system.languagekourse.controller.form;

import by.bsuir.ief.system.languagekourse.FXLoaderController;
import by.bsuir.ief.system.languagekourse.controller.DialogManager;
import by.bsuir.ief.system.languagekourse.controller.table.ChoiceTableController;
import by.bsuir.ief.system.languagekourse.controller.table.TableController;
import by.bsuir.ief.system.languagekourse.model.entity.*;
import by.bsuir.ief.system.languagekourse.model.storage.Repository;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommandShowError;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.group.AddStudentByGroup;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.group.ReadListGroupByCourseCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.requeststudent.UpdateRequestStudentCommand;
import by.bsuir.ief.system.languagekourse.util.ListUtil;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class RequestStudentFormController implements FormController<RequestFullStudentEntity> {

    @FXML
    private AnchorPane rootPane;

    @FXML
    private CheckBox statusRequest;

    @FXML
    private AnchorPane studentForm;

    @FXML
    private AnchorPane courseForm;

    private Stage stage;

    private FormController<StudentEntity> studentEntityFormController;
    private FormController<CourseEntity> courseEntityFormController;

    private RequestFullStudentEntity entity = null;

    @Override
    public void setEntity(RequestFullStudentEntity entity) {

        this.entity = entity;
        if(entity != null){

            statusRequest.setSelected(entity.getIsSuccessfullyRequest() > 0);

            studentEntityFormController.setEntity(entity.getStudentEntity());
            courseEntityFormController.setEntity(entity.getCourseEntity());
        }
    }

    @Override
    public void initEntity() {
        if(isValid()){

            if(entity == null)
                entity = new RequestFullStudentEntity();
        }
    }

    @Override
    public boolean isValid() {

        String errorMessage = "";


        if(studentEntityFormController.getEntity() == null){

            errorMessage += "Нет преподавателя для группы!!!\n";
        }

        if(courseEntityFormController.getEntity() == null){

            errorMessage += "Нет выбранного курса для группы!!!\n";
        }

        if(errorMessage.length() == 0)
            return true;
        else
            DialogManager.showErrorDialog("Ошибка ввода данных",errorMessage);
        return false;
    }

    @Override
    public RequestFullStudentEntity getEntity() {

        return entity;
    }

    @Override
    public void initialize() {

        try {

            studentEntityFormController = FXLoaderController.loadFormController("StudentView");

            studentForm.getChildren().add(studentEntityFormController.getLayout());

            courseEntityFormController = FXLoaderController.loadFormController("CourseView");

            courseForm.getChildren().add(courseEntityFormController.getLayout());

        } catch (Exception e) {
            DialogManager.showErrorDialog("Ошибка подгрузки данных", e.getMessage());
        }
    }

    @FXML
    private void handleOpenGroup(){

        if(entity != null && entity.getIdRequestStudent() > 0) {

            Repository.getResult(new ReadListGroupByCourseCommand(entity.getCourseEntity().getId()))
                    .subscribe(new AbstractCommandShowError<GroupEntity[]>() {
                        @Override
                        public void onNext(GroupEntity[] studentEntities) {

                            try {
                                ChoiceTableController<GroupEntity> choiceTableController = (ChoiceTableController) FXLoaderController.loadTableController("ChoiceTableView");
                                TableController<GroupEntity> groupEntityTableController = FXLoaderController.loadTableController("GroupView");

                                choiceTableController.setController(groupEntityTableController);
                                Stage dialogStage = new Stage();

                                choiceTableController.setMultiSelect(false);

                                Scene scene = new Scene(choiceTableController.getLayout());
                                dialogStage.setScene(scene);
                                choiceTableController.setPrimaryStage(dialogStage);
                                choiceTableController.setList(ListUtil.parseArray(studentEntities));

                                dialogStage.showAndWait();

                                if(choiceTableController.isOk()){

                                    GroupEntity select = choiceTableController.getSelect();

                                    if(select != null) {

                                        StudentsIdByGroupEntity studentsIdByGroupEntity = new StudentsIdByGroupEntity();

                                        List<Integer> integers = new ArrayList<>();

                                        integers.add(entity.getStudentEntity().getIdStudent());

                                        studentsIdByGroupEntity.setIdGroup(select.getIdGroup());
                                        studentsIdByGroupEntity.setIdStudent(integers);

                                        Repository.getResult(new AddStudentByGroup(studentsIdByGroupEntity))
                                                .subscribe(new AbstractCommandShowError<Boolean>() {
                                                    @Override
                                                    public void onNext(Boolean aBoolean) {


                                                        entity.setIsSuccessfullyRequest(1);
                                                        Repository.getResult(new UpdateRequestStudentCommand(entity))
                                                                .subscribe(new AbstractCommandShowError<Boolean>() {
                                                                    @Override
                                                                    public void onNext(Boolean aBoolean) {

                                                                        DialogManager.showInfoDialog("Прошло успешно", "Добавление студентов в группу произошло успешно!");

                                                                        dialogStage.close();
                                                                        stage.close();
                                                                    }
                                                                });
                                                    }
                                                });
                                    }

                                }
                            } catch (Exception ex){

                                DialogManager.showErrorDialog("Ошибка подгрузки данных", ex.getMessage());
                            }
                        }
                    });

        } else {
            DialogManager.showErrorDialog("Error", "Произошла ошибка Запросы на курсы можно обрабатывать только те, что находятся в базе!!!!");
        }
    }

    @FXML
    private void handleClose(){

        stage.close();
    }

    @Override
    public void displayIndicatorProgress() {

    }

    @Override
    public void closeIndicatorProgress() {

    }

    @Override
    public Pane getLayout() {
        return rootPane;
    }

    @Override
    public void setPrimaryStage(Stage primaryStage) {

        stage = primaryStage;
    }
}
