package by.bsuir.ief.system.languagekourse.controller.bottombar;

import by.bsuir.ief.system.languagekourse.controller.CreateEditDeleteBottomController;
import by.bsuir.ief.system.languagekourse.controller.DialogManager;
import by.bsuir.ief.system.languagekourse.controller.form.FormController;
import by.bsuir.ief.system.languagekourse.controller.table.TableController;
import by.bsuir.ief.system.languagekourse.model.entity.TimeTableEntity;
import by.bsuir.ief.system.languagekourse.model.storage.Repository;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommandShowError;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.timetable.AddTimeTableCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.timetable.DeleteTimeTableCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.timetable.UpdateTimeTableCommand;

public class BottomBarTimeTable implements CreateEditDeleteBottomController.CallbackBottomBarManipulation {

    private final int idGroup;

    private final TableController<TimeTableEntity> tableController;
    private final FormController<TimeTableEntity> formController;

    public BottomBarTimeTable(int idGroup,
                              TableController<TimeTableEntity> tableController,
                              FormController<TimeTableEntity> formController) {
        this.idGroup = idGroup;
        this.tableController = tableController;
        this.formController = formController;
    }

    @Override
    public void onCreate() {

        if (formController.isValid()) {
            formController.initEntity();

            TimeTableEntity timeTableEntity = formController.getEntity();
            timeTableEntity.setIdGroup(idGroup);

            Repository.getResult(new AddTimeTableCommand(timeTableEntity))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                        @Override
                        public void onNext(Boolean aBoolean) {
                            DialogManager.showInfoDialog("Операция завершена успешно!", "Создание завершено успешно");
                            Repository.getTimeTable(tableController, idGroup);
                        }
                    });
        }
    }

    @Override
    public void onEdit() {
        if (formController.isValid()) {
            formController.initEntity();

            Repository.getResult(new UpdateTimeTableCommand(formController.getEntity()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                        @Override
                        public void onNext(Boolean aBoolean) {

                            DialogManager.showInfoDialog("Операция завершена успешно!", "Редактирование завершено успешно");
                            Repository.getTimeTable(tableController, idGroup);
                        }
                    });
        }
    }

    @Override
    public void onDelete() {

        TimeTableEntity entity = formController.getEntity();

        if(entity != null) {
            Repository.getResult(new DeleteTimeTableCommand(formController.getEntity().getId()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                        @Override
                        public void onNext(Boolean aBoolean) {
                            DialogManager.showInfoDialog("Операция завершена успешно!", "Удаление завершено успешно");
                            Repository.getTimeTable(tableController, idGroup);
                        }
                    });
        } else {

            DialogManager.showWarningDialog("Ошибка!", " Не выбран элемент для удаления!!");
        }

    }
}
