package by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.requeststudent;

import by.bsuir.ief.system.languagekourse.model.entity.RequestFullStudentEntity;
import by.bsuir.ief.system.languagekourse.model.storage.ConnectToServer;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommand;
import com.sun.istack.internal.NotNull;
import connection.client.Request;
import connection.client.requestbody.JsonRequestBody;
import io.reactivex.Observable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;

public class UpdateRequestStudentCommand extends AbstractCommand<Boolean> {

    @NotNull
    private final RequestFullStudentEntity entity;

    public UpdateRequestStudentCommand(@NotNull final RequestFullStudentEntity entity) {
        this.entity = entity;
    }

    @Override
    protected Request getRequest() throws Exception {
        Request.Builder builder = new Request.Builder()
                .body(new JsonRequestBody(entity))
                .addMethod("updateRequestStudent");

        builder = ConnectToServer.setConnectData(builder);

        return builder.build();
    }

    @Override
    public Observable<Boolean> execute() {
        return super.getObservable()
                .map(response -> response.isSuccessfully())
                .observeOn(JavaFxScheduler.platform());
    }
}
