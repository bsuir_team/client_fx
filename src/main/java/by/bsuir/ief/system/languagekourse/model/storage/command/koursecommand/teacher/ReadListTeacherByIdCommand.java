package by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.teacher;

import by.bsuir.ief.system.languagekourse.model.entity.TeacherEntity;
import by.bsuir.ief.system.languagekourse.model.storage.ConnectToServer;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommand;
import com.google.gson.Gson;
import connection.client.Request;
import connection.client.requestbody.StringRequestBody;
import io.reactivex.Observable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;

public class ReadListTeacherByIdCommand extends AbstractCommand<TeacherEntity> {
    private final int id;

    public ReadListTeacherByIdCommand(int id) {
        this.id = id;
    }

    @Override
    protected Request getRequest() throws Exception {
        Request.Builder builder = new Request.Builder()
                .body(new StringRequestBody(String.valueOf(id)))
                .addMethod("readByIdTeacher");

        builder = ConnectToServer.setConnectData(builder);

        return builder.build();
    }

    @Override
    public Observable<TeacherEntity> execute() {
        return super.getObservable()
                .map(response -> new Gson().fromJson(new String(response.getBody().bytes()), TeacherEntity.class))
                .observeOn(JavaFxScheduler.platform());
    }
}
