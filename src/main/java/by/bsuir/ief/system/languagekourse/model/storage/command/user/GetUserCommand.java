package by.bsuir.ief.system.languagekourse.model.storage.command.user;

import by.bsuir.ief.system.languagekourse.model.entity.UserEntity;
import by.bsuir.ief.system.languagekourse.model.storage.ConnectToServer;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommand;
import com.google.gson.Gson;
import connection.client.Request;
import connection.client.requestbody.NoContentRequestBody;
import io.reactivex.Observable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;

public class GetUserCommand extends AbstractCommand<UserEntity> {

    @Override
    protected Request getRequest() throws Exception {
        Request.Builder builder = new Request.Builder()
                .body(new NoContentRequestBody())
                .addMethod("getUser");

        builder = ConnectToServer.setConnectData(builder);

        return builder.build();
    }

    @Override
    public Observable<UserEntity> execute() {
        return super.getObservable()
                .map(response -> new Gson().fromJson(new String(response.getBody().bytes()), UserEntity.class))
                .observeOn(JavaFxScheduler.platform());
    }
}
