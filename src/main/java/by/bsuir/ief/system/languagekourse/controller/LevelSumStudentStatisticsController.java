package by.bsuir.ief.system.languagekourse.controller;

import by.bsuir.ief.system.languagekourse.model.entity.LevelSumStudentEntity;
import by.bsuir.ief.system.languagekourse.util.ListUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.AnchorPane;

import java.util.ArrayList;
import java.util.List;

public class LevelSumStudentStatisticsController {

    @FXML
    private AnchorPane rootPane;

    @FXML
    private BarChart<String, Integer> barChart;

    @FXML
    private CategoryAxis xAxis;

    private ObservableList<String> stringObservableList = FXCollections.observableArrayList();

    @FXML
    private void initialize() {

    }

    public AnchorPane getRootPane() {
        return rootPane;
    }

    /**
     * Sets the levelSumStudentList to show the statistics for.
     *
     * @param levelSumStudentList
     */
    public void setLevelSumStudentData(List<LevelSumStudentEntity> levelSumStudentList) {

        List<String> names = new ArrayList<>();

        for(LevelSumStudentEntity levelSumStudentEntity: levelSumStudentList){

            names.add(levelSumStudentEntity.getNameLevel());
        }

        stringObservableList = ListUtil.parseListToObserv(stringObservableList, names);

        xAxis.setCategories(stringObservableList);


        XYChart.Series<String, Integer> series = new XYChart.Series<>();

        // Create a XYChart.Data object for each month. Add it to the series.
        for (int i = 0; i < levelSumStudentList.size(); i++) {
            series.getData().add(new XYChart.Data<>(stringObservableList.get(i), levelSumStudentList.get(i).getSumStudent()));
        }

        barChart.getData().add(series);
    }
}
