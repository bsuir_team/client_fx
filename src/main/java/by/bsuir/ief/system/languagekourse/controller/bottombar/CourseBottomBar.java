package by.bsuir.ief.system.languagekourse.controller.bottombar;

import by.bsuir.ief.system.languagekourse.controller.CreateEditDeleteBottomController;
import by.bsuir.ief.system.languagekourse.controller.DialogManager;
import by.bsuir.ief.system.languagekourse.controller.form.FormController;
import by.bsuir.ief.system.languagekourse.controller.table.TableController;
import by.bsuir.ief.system.languagekourse.model.entity.CourseEntity;
import by.bsuir.ief.system.languagekourse.model.storage.Repository;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommandShowError;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.course.AddCourseCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.course.DeleteCourseCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.course.UpdateCourseCommand;

public class CourseBottomBar implements CreateEditDeleteBottomController.CallbackBottomBarManipulation {

    private final TableController<CourseEntity> tableController;
    private final FormController<CourseEntity> formController;

    public CourseBottomBar(TableController<CourseEntity> tableController,
                           FormController<CourseEntity> formController) {
        this.tableController = tableController;
        this.formController = formController;
    }

    @Override
    public void onCreate() {

        if (formController.isValid()) {
            formController.initEntity();
            Repository.getResult(new AddCourseCommand(formController.getEntity()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                @Override
                public void onNext(Boolean aBoolean) {
                    DialogManager.showInfoDialog("Операция завершена успешно!", "Создание завершено успешно");
                    Repository.getCourse(tableController);
                }
            });
        }
    }

    @Override
    public void onEdit() {
        if (formController.isValid()) {
            formController.initEntity();

            Repository.getResult(new UpdateCourseCommand(formController.getEntity()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                @Override
                public void onNext(Boolean aBoolean) {

                    DialogManager.showInfoDialog("Операция завершена успешно!", "Редактирование завершено успешно");
                    Repository.getCourse(tableController);
                }
            });
        }
    }

    @Override
    public void onDelete() {

        CourseEntity entity = formController.getEntity();

        if(entity != null) {
            Repository.getResult(new DeleteCourseCommand(formController.getEntity().getId()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                        @Override
                        public void onNext(Boolean aBoolean) {
                            DialogManager.showInfoDialog("Операция завершена успешно!", "Удаление завершено успешно");
                            Repository.getCourse(tableController);
                        }
                    });
        } else {

            DialogManager.showWarningDialog("Ошибка!", " Не выбран элемент для удаления!!");
        }

    }
}
