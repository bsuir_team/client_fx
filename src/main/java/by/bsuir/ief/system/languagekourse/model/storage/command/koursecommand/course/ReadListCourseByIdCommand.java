package by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.course;

import by.bsuir.ief.system.languagekourse.model.entity.CourseEntity;
import by.bsuir.ief.system.languagekourse.model.storage.ConnectToServer;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommand;
import com.google.gson.Gson;
import connection.client.Request;
import connection.client.requestbody.StringRequestBody;
import io.reactivex.Observable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;

public class ReadListCourseByIdCommand extends AbstractCommand<CourseEntity> {
    private final int id;

    public ReadListCourseByIdCommand(int id) {
        this.id = id;
    }

    @Override
    protected Request getRequest() throws Exception {
        Request.Builder builder = new Request.Builder()
                .body(new StringRequestBody(String.valueOf(id)))
                .addMethod("readByIdCourse");

        builder = ConnectToServer.setConnectData(builder);

        return builder.build();
    }

    @Override
    public Observable<CourseEntity> execute() {
        return super.getObservable()
                .map(response -> new Gson().fromJson(new String(response.getBody().bytes()), CourseEntity.class))
                .observeOn(JavaFxScheduler.platform());
    }
}
