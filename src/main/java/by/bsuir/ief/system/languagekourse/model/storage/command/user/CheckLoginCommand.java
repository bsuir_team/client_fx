package by.bsuir.ief.system.languagekourse.model.storage.command.user;

import by.bsuir.ief.system.languagekourse.model.storage.ConnectToServer;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommand;
import com.sun.istack.internal.NotNull;
import connection.client.Request;
import connection.client.Response;
import connection.client.requestbody.StringRequestBody;
import io.reactivex.Observable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;

public class CheckLoginCommand extends AbstractCommand<Response> {

    @NotNull
    private final String login;

    public CheckLoginCommand(String login) {
        this.login = login;
    }

    @Override
    protected Request getRequest() throws Exception {
        Request.Builder builder = new Request.Builder()
                .body(new StringRequestBody(login))
                .addMethod("checklogin");

        builder = ConnectToServer.setConnectData(builder);

        return builder.build();
    }

    @Override
    public Observable<Response> execute() {
        return super.getObservable()
                .observeOn(JavaFxScheduler.platform());//результат будет возвращаться в UI поток;
    }
}
