package by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.group;


import by.bsuir.ief.system.languagekourse.model.entity.GroupEntity;
import by.bsuir.ief.system.languagekourse.model.storage.ConnectToServer;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommand;
import com.sun.istack.internal.NotNull;
import connection.client.Request;
import connection.client.requestbody.JsonRequestBody;
import io.reactivex.Observable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;

public class UpdateGroupCommand extends AbstractCommand<Boolean> {
    @NotNull
    private final GroupEntity entity;

    public UpdateGroupCommand(@NotNull final GroupEntity entity) {
        this.entity = entity;
    }

    @Override
    protected Request getRequest() throws Exception {
        Request.Builder builder = new Request.Builder()
                .body(new JsonRequestBody(entity))
                .addMethod("updateGroup");

        builder = ConnectToServer.setConnectData(builder);

        return builder.build();
    }

    @Override
    public Observable<Boolean> execute() {
        return super.getObservable()
                .map(response -> response.isSuccessfully())
                .observeOn(JavaFxScheduler.platform());
    }
}
