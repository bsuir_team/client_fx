package by.bsuir.ief.system.languagekourse.model.entity;

import java.util.Objects;

public class GroupEntity {

    private int idGroup;

    private String nameGroup;

    private CourseEntity courseByIdCourse;
    private TeacherEntity teacherByTeacherId;

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public String getNameGroup() {
        return nameGroup;
    }

    public void setNameGroup(String nameGroup) {
        this.nameGroup = nameGroup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupEntity that = (GroupEntity) o;
        return idGroup == that.idGroup &&
                Objects.equals(nameGroup, that.nameGroup);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idGroup, nameGroup);
    }

    public CourseEntity getCourseByIdCourse() {
        return courseByIdCourse;
    }

    public void setCourseByIdCourse(CourseEntity courseByIdCourse) {
        this.courseByIdCourse = courseByIdCourse;
    }

    public TeacherEntity getTeacherByTeacherId() {
        return teacherByTeacherId;
    }

    public void setTeacherByTeacherId(TeacherEntity teacherByTeacherId) {
        this.teacherByTeacherId = teacherByTeacherId;
    }
}
