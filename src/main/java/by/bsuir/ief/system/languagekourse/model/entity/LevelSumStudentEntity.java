package by.bsuir.ief.system.languagekourse.model.entity;

import java.util.Objects;

public class LevelSumStudentEntity {

    private String nameLevel;
    private Integer sumStudent;

    public String getNameLevel() {
        return nameLevel;
    }

    public void setNameLevel(String nameLevel) {
        this.nameLevel = nameLevel;
    }

    public Integer getSumStudent() {
        return sumStudent;
    }

    public void setSumStudent(Integer sumStudent) {
        this.sumStudent = sumStudent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LevelSumStudentEntity that = (LevelSumStudentEntity) o;
        return Objects.equals(nameLevel, that.nameLevel) &&
                Objects.equals(sumStudent, that.sumStudent);
    }

    @Override
    public int hashCode() {

        return Objects.hash(nameLevel, sumStudent);
    }
}
