package by.bsuir.ief.system.languagekourse.controller.table;

import by.bsuir.ief.system.languagekourse.controller.DialogManager;
import by.bsuir.ief.system.languagekourse.model.entity.StudentEntity;
import by.bsuir.ief.system.languagekourse.util.ListUtil;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class StudentTableController implements TableController<StudentEntity> {

    @FXML
    private AnchorPane rootView;
    @FXML
    private TableView<StudentEntity> clientEntityTableView;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private TableColumn<StudentEntity, String> surname;
    @FXML
    private TableColumn<StudentEntity, String> name;
    @FXML
    private TableColumn<StudentEntity, String> patronymic;
    @FXML
    private TableColumn<StudentEntity, String> number;

    private Stage primaryStage;
    private boolean isMultiSelect;

    private ObservableList<StudentEntity> entityObservableList = FXCollections.observableArrayList();
    private CallbackSelected<StudentEntity> callbackSelected = null;

    @Override
    public StudentEntity getSelect() {
        return clientEntityTableView.getSelectionModel().getSelectedItem();
    }

    @Override
    public List<StudentEntity> getSelected() {
        return clientEntityTableView.getSelectionModel().getSelectedItems();
    }

    @Override
    public void setList(List<StudentEntity> list) {

        entityObservableList = ListUtil.parseListToObserv(entityObservableList, list);

        initTable();

        closeIndicatorProgress();
    }

    @Override
    public void setMultiSelect(boolean multiSelect) {

        isMultiSelect = multiSelect;

        initTable();
    }

    @Override
    public void setCallbackSelect(CallbackSelected<StudentEntity> callbackSelect) {
        this.callbackSelected = callbackSelect;
    }

    @Override
    public void initialize() {

        /*rootView.setBackground(Style.getBackground());
        clientEntityTableView.setBackground(Style.getBackground());*/
    }

    @Override
    public void displayIndicatorProgress() {
        clientEntityTableView.setVisible(false);
        progressIndicator.setVisible(true);
    }

    @Override
    public void closeIndicatorProgress() {
        clientEntityTableView.setVisible(true);
        progressIndicator.setVisible(false);
    }

    @Override
    public Pane getLayout() {
        return rootView;
    }

    @Override
    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    private void initTable(){

        clientEntityTableView.getItems().clear();

        surname.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSurname()));
        name.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        patronymic.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPatronymic()));

        number.setCellValueFactory(cellData -> new SimpleStringProperty((cellData.getValue().getNumber())));

        clientEntityTableView.getSelectionModel().selectedItemProperty().
                addListener(((observable, oldValue, newValue) -> {
                    if(callbackSelected != null) callbackSelected.onSelectItem(newValue);
                }));

        if(isMultiSelect)
            clientEntityTableView.getSelectionModel().setSelectionMode(
                    SelectionMode.MULTIPLE
            );

        clientEntityTableView.setItems(entityObservableList);
    }

    @FXML
    private void export(){

        FileChooser fileChooser = new FileChooser();

        // Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "pdf files (*.pdf)", "*.pdf");
        fileChooser.getExtensionFilters().add(extFilter);

        // Show save file dialog
        File file = fileChooser.showSaveDialog(primaryStage);

        if (file != null) {
            // Make sure it has the correct extension
            if (!file.getPath().endsWith(".pdf")) {
                file = new File(file.getPath() + ".pdf");
            }

            File finalFile = file;
            new Thread(()->{ try  {
                Document document = new Document();
                // step 2
                PdfWriter.getInstance(document, new FileOutputStream(finalFile));
                // step 3
                document.open();
                // step 4
                document.add(createPdfTable());
                // step 5
                document.close();
            } catch (IOException | DocumentException e) {
                Platform.runLater(()->{
                    DialogManager.showErrorDialog("Ошибка при записи в файл", "Ошибка при записи в файл " + e.getMessage());});
            }
            }).start();

        }
    }

    PdfPTable createPdfTable() throws IOException, DocumentException {

        BaseFont font = BaseFont.createFont("/font/DejaVuSans.ttf", "cp1251", BaseFont.EMBEDDED);
        BaseFont  Twofont = BaseFont.createFont("/font/DejaVuSans.ttf", "cp1251", BaseFont.NOT_EMBEDDED);
        PdfPTable table = new PdfPTable(4);

        PdfPCell c1 = new PdfPCell(new Phrase("Фамилия", new Font(font, 14)));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Имя", new Font(font, 14)));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);

        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Отчество", new Font(font, 14)));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Номер", new Font(font, 14)));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        table.setHeaderRows(1);

        for(StudentEntity routeEntity: entityObservableList){
            table.addCell(new Phrase(routeEntity.getSurname(), new Font(Twofont, 12)));
            table.addCell(new Phrase(routeEntity.getName(), new Font(font, 12)));
            table.addCell(new Phrase(routeEntity.getPatronymic(), new Font(Twofont, 12)));
            table.addCell(new Phrase(routeEntity.getNumber(), new Font(Twofont, 12)));
        }

        return table;
    }
}
