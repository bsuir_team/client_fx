package by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.level;

import by.bsuir.ief.system.languagekourse.model.entity.LevelSumStudentEntity;
import by.bsuir.ief.system.languagekourse.model.storage.ConnectToServer;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommand;
import com.google.gson.Gson;
import connection.client.Request;
import connection.client.requestbody.NoContentRequestBody;
import io.reactivex.Observable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;

public class ReadLevelSumStudentCommand extends AbstractCommand<LevelSumStudentEntity[]>{

    @Override
    protected Request getRequest() throws Exception {
        Request.Builder builder = new Request.Builder()
                .body(new NoContentRequestBody())
                .addMethod("readLevelSumStudentCommand");

        builder = ConnectToServer.setConnectData(builder);

        return builder.build();
    }

    @Override
    public Observable<LevelSumStudentEntity[]> execute() {
        return super.getObservable()
                .map(response -> new Gson().fromJson(new String(response.getBody().bytes()), LevelSumStudentEntity[].class))
                .observeOn(JavaFxScheduler.platform());
    }
}
