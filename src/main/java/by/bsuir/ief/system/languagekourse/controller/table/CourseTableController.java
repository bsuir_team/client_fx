package by.bsuir.ief.system.languagekourse.controller.table;

import by.bsuir.ief.system.languagekourse.model.entity.CourseEntity;
import by.bsuir.ief.system.languagekourse.util.ListUtil;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.List;

public class CourseTableController implements TableController<CourseEntity> {

    @FXML
    private AnchorPane rootView;
    @FXML
    private TableView<CourseEntity> tableView;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private TableColumn<CourseEntity, String> name;
    @FXML
    private TableColumn<CourseEntity, Double> priceTableColumn;
    @FXML
    private TableColumn<CourseEntity, Integer> hoursLessonCourse;
    @FXML
    private TableColumn<CourseEntity, String> levelCourse;
    @FXML
    private TableColumn<CourseEntity, String> languageCourse;

    private ObservableList<CourseEntity> entityObservableList = FXCollections.observableArrayList();
    private CallbackSelected<CourseEntity> callbackSelected = null;
    private Stage primaryStage;
    private boolean isMultiSelect;

    @Override
    public CourseEntity getSelect() {
        return tableView.getSelectionModel().getSelectedItem();
    }

    @Override
    public List<CourseEntity> getSelected() {
        return tableView.getSelectionModel().getSelectedItems();
    }

    @Override
    public void setList(List<CourseEntity> list) {

        entityObservableList = ListUtil.parseListToObserv(entityObservableList, list);

        initTable();

        closeIndicatorProgress();

    }

    @Override
    public void setMultiSelect(boolean multiSelect) {

        isMultiSelect = multiSelect;

        initTable();
    }

    @Override
    public void setCallbackSelect(CallbackSelected<CourseEntity> callbackSelect) {
        this.callbackSelected = callbackSelect;
    }

    @Override
    public void initialize() {
       /* rootView.setBackground(Style.getBackground());
        tableView.setBackground(Style.getBackground());*/
    }

    @Override
    public void displayIndicatorProgress() {
        tableView.setVisible(false);
        progressIndicator.setVisible(true);
    }

    @Override
    public void closeIndicatorProgress() {
        tableView.setVisible(true);
        progressIndicator.setVisible(false);
    }

    @Override
    public Pane getLayout() {
        return rootView;
    }

    @Override
    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    private void initTable(){

        tableView.getItems().clear();

        name.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        priceTableColumn.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getPrice()));
        hoursLessonCourse.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getHoursLesson()));
        levelCourse.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getLevelByIdLevel().getName()));
        languageCourse.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getLanguageByIdLanguage().getName()));

        tableView.getSelectionModel().selectedItemProperty().
                addListener(((observable, oldValue, newValue) -> {
                    if(callbackSelected != null) callbackSelected.onSelectItem(newValue);
                }));

        if(isMultiSelect)
            tableView.getSelectionModel().setSelectionMode(
                    SelectionMode.MULTIPLE
            );

        tableView.setItems(entityObservableList);
    }
}
