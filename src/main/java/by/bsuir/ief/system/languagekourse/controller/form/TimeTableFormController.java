package by.bsuir.ief.system.languagekourse.controller.form;

import by.bsuir.ief.system.languagekourse.controller.DialogManager;
import by.bsuir.ief.system.languagekourse.model.entity.TimeTableEntity;
import by.bsuir.ief.system.languagekourse.util.DateUtil;
import by.bsuir.ief.system.languagekourse.util.Style;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.Date;

public class TimeTableFormController implements FormController<TimeTableEntity> {

    @FXML
    private Pane rootPane;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private GridPane contentView;
    @FXML
    private TextField roomRequred;
    @FXML
    private TextField adressRequired;
    @FXML
    private DatePicker birthdayRequired;

    private TimeTableEntity entity = null;

    private boolean isEditable;

    @Override
    public void initialize() {

    }

    @Override
    public void setEntity(TimeTableEntity entity) {

        this.entity = entity;
        if(entity != null){

            roomRequred.setText(String.valueOf(entity.getRoom()));
            adressRequired.setText(entity.getAdress());
            birthdayRequired.setValue(DateUtil.getLocalDate(entity.getDateTime() == null ? new Date(): entity.getDateTime()));

        } else {

            birthdayRequired.setValue(DateUtil.getLocalDate(new Date()));
        }
    }

    @Override
    public void initEntity() {
        if(isValid()){

            if(entity == null)
                entity = new TimeTableEntity();

            entity.setRoom(Integer.parseInt(roomRequred.getText()));
            entity.setAdress(adressRequired.getText());
            entity.setDateTime(DateUtil.getDate(birthdayRequired.getValue()));
        }
    }

    @Override
    public boolean isValid() {

        String errorMessage = "";
        //Required field

        if (roomRequred.getText().length() < 1){
            errorMessage += "Поле \"Аудитория\" обязательно к заполнению\n";
            roomRequred.setStyle(Style.getErrorStyle());
        } else {
            roomRequred.setStyle(null);
        }

        if(adressRequired.getText().length() < 1) {
            errorMessage += "Поле \"Адресс\" обязательно к заполнению\n";
            adressRequired.setStyle(Style.getErrorStyle());
        } else  {
            adressRequired.setStyle(null);
        }

        if(birthdayRequired.getEditor().getText().length() < 1) {
            errorMessage += "Поле \"Дата и время проведения\" обязательно к заполнению\n";
            birthdayRequired.setStyle(Style.getErrorStyle());
        }  else {
            birthdayRequired.setStyle(null);
        }

        if(errorMessage.length() == 0)
            return true;
        else
            DialogManager.showErrorDialog("Ошибка ввода данных",errorMessage);
        return false;
    }

    /**
     * Вызывать только после метода {@link #initEntity()}
     * @return
     */
    @Override
    public TimeTableEntity getEntity() {

        return entity;
    }


    @Override
    public void displayIndicatorProgress() {
        contentView.setVisible(false);
        progressIndicator.setVisible(true);
    }

    @Override
    public void closeIndicatorProgress() {
        contentView.setVisible(true);
        progressIndicator.setVisible(false);
    }

    @Override
    public Pane getLayout() {
        return rootPane;
    }

    @Override
    public void setPrimaryStage(Stage primaryStage) {

    }
}
