package by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.requeststudent;

import by.bsuir.ief.system.languagekourse.model.entity.RequestFullStudentEntity;
import by.bsuir.ief.system.languagekourse.model.storage.ConnectToServer;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommand;
import com.google.gson.GsonBuilder;
import connection.client.Request;
import connection.client.requestbody.NoContentRequestBody;
import io.reactivex.Observable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;

public class ReadListRequestStudentCommand extends AbstractCommand<RequestFullStudentEntity[]> {

    @Override
    protected Request getRequest() throws Exception {
        Request.Builder builder = new Request.Builder()
                .body(new NoContentRequestBody())
                .addMethod("readRequestStudent");

        builder = ConnectToServer.setConnectData(builder);

        return builder.build();
    }

    @Override
    public Observable<RequestFullStudentEntity []> execute() {
        return super.getObservable()
                .map(response -> new GsonBuilder().setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz").create().fromJson(new String(response.getBody().bytes()), RequestFullStudentEntity[].class))
                .observeOn(JavaFxScheduler.platform());
    }
}
