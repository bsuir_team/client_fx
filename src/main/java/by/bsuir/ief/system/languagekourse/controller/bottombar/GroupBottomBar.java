package by.bsuir.ief.system.languagekourse.controller.bottombar;

import by.bsuir.ief.system.languagekourse.controller.CreateEditDeleteBottomController;
import by.bsuir.ief.system.languagekourse.controller.DialogManager;
import by.bsuir.ief.system.languagekourse.controller.form.FormController;
import by.bsuir.ief.system.languagekourse.controller.table.TableController;
import by.bsuir.ief.system.languagekourse.model.entity.GroupEntity;
import by.bsuir.ief.system.languagekourse.model.storage.Repository;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommandShowError;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.group.AddGroupCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.group.DeleteGroupCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.group.UpdateGroupCommand;

public class GroupBottomBar implements CreateEditDeleteBottomController.CallbackBottomBarManipulation {

    private final TableController<GroupEntity> tableController;
    private final FormController<GroupEntity> formController;

    public GroupBottomBar(TableController<GroupEntity> tableController,
                          FormController<GroupEntity> formController) {
        this.tableController = tableController;
        this.formController = formController;
    }

    @Override
    public void onCreate() {

        if (formController.isValid()) {
            formController.initEntity();
            Repository.getResult(new AddGroupCommand(formController.getEntity()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                        @Override
                        public void onNext(Boolean aBoolean) {
                            DialogManager.showInfoDialog("Операция завершена успешно!", "Создание завершено успешно");
                            Repository.getGroup(tableController);
                        }
                    });
        }
    }

    @Override
    public void onEdit() {
        if (formController.isValid()) {
            formController.initEntity();

            Repository.getResult(new UpdateGroupCommand(formController.getEntity()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                        @Override
                        public void onNext(Boolean aBoolean) {

                            DialogManager.showInfoDialog("Операция завершена успешно!", "Редактирование завершено успешно");
                            Repository.getGroup(tableController);
                        }
                    });
        }
    }

    @Override
    public void onDelete() {

        GroupEntity entity = formController.getEntity();

        if(entity != null) {
            Repository.getResult(new DeleteGroupCommand(formController.getEntity().getIdGroup()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                        @Override
                        public void onNext(Boolean aBoolean) {
                            DialogManager.showInfoDialog("Операция завершена успешно!", "Удаление завершено успешно");
                            Repository.getGroup(tableController);
                        }
                    });
        } else {

            DialogManager.showWarningDialog("Ошибка!", " Не выбран элемент для удаления!!");
        }
    }
}
