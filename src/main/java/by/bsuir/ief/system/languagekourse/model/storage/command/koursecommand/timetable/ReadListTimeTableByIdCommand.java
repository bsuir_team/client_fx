package by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.timetable;

import by.bsuir.ief.system.languagekourse.model.entity.TimeTableEntity;
import by.bsuir.ief.system.languagekourse.model.storage.ConnectToServer;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommand;
import com.google.gson.GsonBuilder;
import connection.client.Request;
import connection.client.requestbody.StringRequestBody;
import io.reactivex.Observable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;

public class ReadListTimeTableByIdCommand extends AbstractCommand<TimeTableEntity> {
    private final int id;

    public ReadListTimeTableByIdCommand(int id) {
        this.id = id;
    }

    @Override
    protected Request getRequest() throws Exception {
        Request.Builder builder = new Request.Builder()
                .body(new StringRequestBody(String.valueOf(id)))
                .addMethod("readByIdTimeTable");

        builder = ConnectToServer.setConnectData(builder);

        return builder.build();
    }

    @Override
    public Observable<TimeTableEntity> execute() {
        return super.getObservable()
                .map(response -> new GsonBuilder().setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz")
                        .create().fromJson(new String(response.getBody().bytes()), TimeTableEntity.class))
                .observeOn(JavaFxScheduler.platform());
    }
}
