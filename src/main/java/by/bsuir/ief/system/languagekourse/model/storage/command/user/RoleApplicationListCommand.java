package by.bsuir.ief.system.languagekourse.model.storage.command.user;

import by.bsuir.ief.system.languagekourse.model.entity.RoleApplicationEntity;
import by.bsuir.ief.system.languagekourse.model.storage.ConnectToServer;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommand;
import com.google.gson.Gson;
import connection.client.Request;
import connection.client.requestbody.NoContentRequestBody;
import io.reactivex.Observable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;

public class RoleApplicationListCommand extends AbstractCommand<RoleApplicationEntity[]> {
    @Override
    protected Request getRequest() throws Exception {
        Request.Builder builder = new Request.Builder()
                .body(new NoContentRequestBody())
                .addMethod("getlistroleapplication");

        builder = ConnectToServer.setConnectData(builder);

        return builder.build();
    }

    @Override
    public Observable<RoleApplicationEntity[]> execute() {
        return getObservable()
                .map(response -> new Gson().fromJson(new String(response.getBody().bytes()), RoleApplicationEntity[].class))
                .observeOn(JavaFxScheduler.platform());
    }
}
