package by.bsuir.ief.system.languagekourse.controller;

import by.bsuir.ief.system.languagekourse.FXLoaderController;
import by.bsuir.ief.system.languagekourse.controller.bottombar.*;
import by.bsuir.ief.system.languagekourse.controller.form.FormController;
import by.bsuir.ief.system.languagekourse.controller.table.TableController;
import by.bsuir.ief.system.languagekourse.model.entity.*;
import by.bsuir.ief.system.languagekourse.model.storage.Repository;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommandShowError;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractObserver;
import by.bsuir.ief.system.languagekourse.model.storage.command.ExitCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.student.ReadListStudentCommand;
import by.bsuir.ief.system.languagekourse.util.ListUtil;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class AdminController implements Controller {

    private Stage primaryStage;

    @FXML
    private BorderPane pane;

    @FXML
    private AnchorPane centerPane;

    @FXML
    @Override
    public void initialize() {

      /*  pane.setBackground(Style.getBackground());
        centerPane.setBackground(Style.getBackground());*/
    }

    @Override
    public void displayIndicatorProgress() {

    }

    @Override
    public void closeIndicatorProgress() {

    }

    @Override
    public Pane getLayout() {
        return pane;
    }

    @Override
    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    @FXML
    private void handleWorkUsers(){

        clearPane();

        try {
            TableController<UserEntity> tableController = FXLoaderController.loadTableController("UserTableView");

            FormController<UserEntity> formController = FXLoaderController.loadFormController("UserEditFormView");

            BottomBarController bottomBarController = (BottomBarController) FXLoaderController.loadController("BottomBar");

            CreateEditDeleteBottomController createEditDeleteBottomController = (CreateEditDeleteBottomController) FXLoaderController.loadController("CreateEditDeleteBottomBarView");

            createEditDeleteBottomController.setCallback(new UserCallbackBottomBarManipulation(tableController, formController));
            bottomBarController.setAdminPane(createEditDeleteBottomController.getLayout());

            pane.setBottom(bottomBarController.getLayout());

            Repository.getsUser(tableController);

            tableController.setCallbackSelect(formController::setEntity);

            pane.setCenter(tableController.getLayout());
            pane.setLeft(formController.getLayout());

        } catch (IOException e) {
            DialogManager.showErrorDialog("Error", "Произошла ошибка " + e.getMessage());
        }
    }

    @FXML
    private void handleWorkLanguage() {

        clearPane();

        try {
            TableController<LanguageEntity> tableController = FXLoaderController.loadTableController("LanguageView");

            FormController<LanguageEntity> formController = FXLoaderController.loadFormController("LanguageView");

            BottomBarController bottomBarController = (BottomBarController) FXLoaderController.loadController("BottomBar");

            CreateEditDeleteBottomController createEditDeleteBottomController = (CreateEditDeleteBottomController) FXLoaderController.loadController("CreateEditDeleteBottomBarView");

            createEditDeleteBottomController.setCallback(new LanguageBottomBar(tableController, formController));

            bottomBarController.setAdminPane(createEditDeleteBottomController.getLayout());

            pane.setBottom(bottomBarController.getLayout());

            Repository.getLanguage(tableController);

            tableController.setCallbackSelect(formController::setEntity);

            pane.setCenter(tableController.getLayout());
            pane.setLeft(formController.getLayout());

        } catch (IOException e) {
            DialogManager.showErrorDialog("Error", "Произошла ошибка " + e.getMessage());
        }
    }

   @FXML
    private void handleWorkLevel() {

       clearPane();

        try {
            TableController<LevelEntity> tableController = FXLoaderController.loadTableController("LevelView");

            FormController<LevelEntity> formController = FXLoaderController.loadFormController("LevelView");

            BottomBarController bottomBarController = (BottomBarController) FXLoaderController.loadController("BottomBar");

            CreateEditDeleteBottomController createEditDeleteBottomController = (CreateEditDeleteBottomController) FXLoaderController.loadController("CreateEditDeleteBottomBarView");

            createEditDeleteBottomController.setCallback(new LevelBottomBar(tableController, formController));

            bottomBarController.setAdminPane(createEditDeleteBottomController.getLayout());

            pane.setBottom(bottomBarController.getLayout());

            Repository.getLevel(tableController);

            tableController.setCallbackSelect(formController::setEntity);

            pane.setCenter(tableController.getLayout());
            pane.setLeft(formController.getLayout());

        } catch (IOException e) {
            DialogManager.showErrorDialog("Error", "Произошла ошибка " + e.getMessage());
        }
    }

    @FXML
    private void handleWorkTeacher() {

        clearPane();

        try {
            TableController<TeacherEntity> tableController = FXLoaderController.loadTableController("TeacherView");

            FormController<TeacherEntity> formController = FXLoaderController.loadFormController("TeacherView");

            BottomBarController bottomBarController = (BottomBarController) FXLoaderController.loadController("BottomBar");

            CreateEditDeleteBottomController createEditDeleteBottomController = (CreateEditDeleteBottomController) FXLoaderController.loadController("CreateEditDeleteBottomBarView");

            createEditDeleteBottomController.setCallback(new TeacherBottomBar(tableController, formController));

            bottomBarController.setAdminPane(createEditDeleteBottomController.getLayout());

            pane.setBottom(bottomBarController.getLayout());

            Repository.getsTeacher(tableController);

            tableController.setCallbackSelect(formController::setEntity);

            pane.setCenter(tableController.getLayout());
            pane.setLeft(formController.getLayout());

        } catch (IOException e) {
            DialogManager.showErrorDialog("Error", "Произошла ошибка " + e.getMessage());
        }
    }

    @FXML
    private void handleWorkCourse() {

        clearPane();

        try {
            TableController<CourseEntity> tableController = FXLoaderController.loadTableController("CourseView");

            FormController<CourseEntity> formController = FXLoaderController.loadFormController("CourseView");

            BottomBarController bottomBarController = (BottomBarController) FXLoaderController.loadController("BottomBar");

            CreateEditDeleteBottomController createEditDeleteBottomController = (CreateEditDeleteBottomController) FXLoaderController.loadController("CreateEditDeleteBottomBarView");

            createEditDeleteBottomController.setCallback(new CourseBottomBar(tableController, formController));

            bottomBarController.setAdminPane(createEditDeleteBottomController.getLayout());

            pane.setBottom(bottomBarController.getLayout());

            Repository.getCourse(tableController);

            tableController.setCallbackSelect(formController::setEntity);

            pane.setCenter(tableController.getLayout());
            pane.setLeft(formController.getLayout());

        } catch (IOException e) {
            DialogManager.showErrorDialog("Error", "Произошла ошибка " + e.getMessage());
        }
    }

    @FXML
    private void handleWorkStudent() {

        clearPane();

        try {
            TableController<StudentEntity> tableController = FXLoaderController.loadTableController("StudentView");

            FormController<StudentEntity> formController = FXLoaderController.loadFormController("StudentView");

            BottomBarController bottomBarController = (BottomBarController) FXLoaderController.loadController("BottomBar");

            CreateEditDeleteBottomController createEditDeleteBottomController = (CreateEditDeleteBottomController) FXLoaderController.loadController("CreateEditDeleteBottomBarView");

            createEditDeleteBottomController.setCallback(new StudentBottomBar(tableController, formController));

            bottomBarController.setAdminPane(createEditDeleteBottomController.getLayout());

            pane.setBottom(bottomBarController.getLayout());

            Repository.getStudents(tableController);

            tableController.setCallbackSelect(formController::setEntity);

            pane.setCenter(tableController.getLayout());
            pane.setLeft(formController.getLayout());

        } catch (IOException e) {
            DialogManager.showErrorDialog("Error", "Произошла ошибка " + e.getMessage());
        }
    }

    @FXML
    private void handleWorkGroup(){

        clearPane();

        try {
            TableController<GroupEntity> tableController = FXLoaderController.loadTableController("GroupView");

            FormController<GroupEntity> formController = FXLoaderController.loadFormController("GroupView");

            BottomBarController bottomBarController = (BottomBarController) FXLoaderController.loadController("BottomBar");

            CreateEditDeleteBottomController createEditDeleteBottomController = (CreateEditDeleteBottomController) FXLoaderController.loadController("CreateEditDeleteBottomBarView");

            createEditDeleteBottomController.setCallback(new GroupBottomBar(tableController, formController));

            bottomBarController.setAdminPane(createEditDeleteBottomController.getLayout());

            pane.setBottom(bottomBarController.getLayout());

            Repository.getGroup(tableController);

            tableController.setCallbackSelect(formController::setEntity);

            pane.setRight(tableController.getLayout());
            pane.setLeft(formController.getLayout());

        } catch (IOException e) {
            DialogManager.showErrorDialog("Error", "Произошла ошибка " + e.getMessage());
        }
    }

    @FXML
    private void handleWorkRequestStudent(){

        clearPane();

        try {
            TableController<RequestFullStudentEntity> tableController = FXLoaderController.loadTableController("RequestView");

            BottomBarController bottomBarController = (BottomBarController) FXLoaderController.loadController("BottomBar");

            ButtonView buttonView = (ButtonView) FXLoaderController.loadController("ButtonView");

            bottomBarController.setAdminPane(buttonView.getLayout());
            buttonView.setText("Приступить к обработке");

            buttonView.setOnClick(() -> {

                if(tableController.getSelect() != null) {

                    try {

                        Stage stage = new Stage();
                        FormController<RequestFullStudentEntity> formController = FXLoaderController.loadFormController("RequestView");

                        stage.initModality(Modality.WINDOW_MODAL);
                        stage.setScene(new Scene(formController.getLayout()));

                        formController.setEntity(tableController.getSelect());

                        formController.setPrimaryStage(stage);

                        stage.showAndWait();

                        Repository.getRequestStudents(tableController);

                    } catch (IOException e) {
                        DialogManager.showErrorDialog("Error", "Произошла ошибка " + e.getMessage());
                    }
                }
            });

            pane.setBottom(bottomBarController.getLayout());
            Repository.getRequestStudents(tableController);

            pane.setRight(tableController.getLayout());

        } catch (IOException e) {
            DialogManager.showErrorDialog("Error", "Произошла ошибка " + e.getMessage());
        }
    }

    @FXML
    private void handleShowstatisticBirthday(){

        Repository.getResult(new ReadListStudentCommand())
                .subscribe(new AbstractCommandShowError<StudentEntity[]>() {
                    @Override
                    public void onNext(StudentEntity[] studentEntities) {

                        try {
                            FXMLLoader loader = FXLoaderController.load("BirthdayStatistics");

                            BirthdayStatisticsController controller = loader.getController();
                            Stage dialogStage = new Stage();
                            dialogStage.setTitle("Статистика студетов по м есяцам рождения");
                            dialogStage.initModality(Modality.WINDOW_MODAL);
                            dialogStage.initOwner(primaryStage);
                            Scene scene = new Scene(controller.getRootPane());
                            dialogStage.setScene(scene);

                            controller.setPersonData(ListUtil.parseArray(studentEntities));
                            dialogStage.show();

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                });
    }

    private void clearPane(){

        pane.setLeft(null);
        pane.setRight(null);
        pane.setCenter(null);
        pane.setBottom(null);
    }

    @FXML
    private void handleExit(){
        try {
            Repository.getResult(new ExitCommand()).subscribe(new AbstractObserver<Boolean>() {
                @Override
                public void onNext(Boolean aBoolean) {

                }

                @Override
                public void onError(Throwable throwable) {

                }
            });
            FXMLLoader loader = FXLoaderController.load("ConnectionView");

            ConnectionViewController controller = loader.getController();

            Scene scene = new Scene(controller.getRootLayout());
            primaryStage.setScene(scene);

            controller.setPrimaryStage(this.primaryStage);
            primaryStage.show();

        } catch (IOException e) {
            DialogManager.showErrorDialog(
                    "Ошибка в файле",
                    "Непредвиденная ошибка!");
        }
    }
}
