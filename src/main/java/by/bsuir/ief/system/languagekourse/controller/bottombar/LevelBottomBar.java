package by.bsuir.ief.system.languagekourse.controller.bottombar;

import by.bsuir.ief.system.languagekourse.controller.CreateEditDeleteBottomController;
import by.bsuir.ief.system.languagekourse.controller.DialogManager;
import by.bsuir.ief.system.languagekourse.controller.form.FormController;
import by.bsuir.ief.system.languagekourse.controller.table.TableController;
import by.bsuir.ief.system.languagekourse.model.entity.LevelEntity;
import by.bsuir.ief.system.languagekourse.model.storage.Repository;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommandShowError;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.level.AddLevelCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.level.DeleteLevelCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.level.UpdateLevelCommand;

public class LevelBottomBar implements CreateEditDeleteBottomController.CallbackBottomBarManipulation {

    private final TableController<LevelEntity> tableController;
    private final FormController<LevelEntity> formController;

    public LevelBottomBar(TableController<LevelEntity> tableController,
                          FormController<LevelEntity> formController) {
        this.tableController = tableController;
        this.formController = formController;
    }

    @Override
    public void onCreate() {

        if (formController.isValid()) {
            formController.initEntity();
            Repository.getResult(new AddLevelCommand(formController.getEntity()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                @Override
                public void onNext(Boolean aBoolean) {
                    DialogManager.showInfoDialog("Операция завершена успешно!", "Создание завершено успешно");
                    Repository.getLevel(tableController);
                }
            });
        }
    }

    @Override
    public void onEdit() {
        if (formController.isValid()) {
            formController.initEntity();

            Repository.getResult(new UpdateLevelCommand(formController.getEntity()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                @Override
                public void onNext(Boolean aBoolean) {

                    DialogManager.showInfoDialog("Операция завершена успешно!", "Редактирование завершено успешно");
                    Repository.getLevel(tableController);
                }
            });
        }
    }

    @Override
    public void onDelete() {

        LevelEntity entity = formController.getEntity();
        if(entity != null) {
            Repository.getResult(new DeleteLevelCommand(formController.getEntity().getId()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                        @Override
                        public void onNext(Boolean aBoolean) {
                            DialogManager.showInfoDialog("Операция завершена успешно!", "Удаление завершено успешно");
                            Repository.getLevel(tableController);
                        }
                    });
        } else {

            DialogManager.showWarningDialog("Ошибка!", " Не выбран элемент для удаления!!");
        }

    }
}
