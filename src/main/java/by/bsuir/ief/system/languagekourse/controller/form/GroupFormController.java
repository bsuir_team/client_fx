package by.bsuir.ief.system.languagekourse.controller.form;

import by.bsuir.ief.system.languagekourse.FXLoaderController;
import by.bsuir.ief.system.languagekourse.controller.BottomBarController;
import by.bsuir.ief.system.languagekourse.controller.CreateEditDeleteBottomController;
import by.bsuir.ief.system.languagekourse.controller.DialogManager;
import by.bsuir.ief.system.languagekourse.controller.NewWindowWorking;
import by.bsuir.ief.system.languagekourse.controller.bottombar.BottomBarTimeTable;
import by.bsuir.ief.system.languagekourse.controller.table.ChoiceTableController;
import by.bsuir.ief.system.languagekourse.controller.table.TableController;
import by.bsuir.ief.system.languagekourse.model.entity.*;
import by.bsuir.ief.system.languagekourse.model.storage.Repository;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommandShowError;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.course.ReadListCourseCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.group.AddStudentByGroup;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.student.ReadListStudentCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.teacher.ReadListTeacherCommand;
import by.bsuir.ief.system.languagekourse.util.ListUtil;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GroupFormController implements FormController<GroupEntity> {

    @FXML
    private AnchorPane rootPane;

    @FXML
    private TextField name;

    @FXML
    private Pane dataPane;

    @FXML
    private AnchorPane teacherForm;

    @FXML
    private AnchorPane courseForm;

    private FormController<TeacherEntity> teacherEntityFormController;
    private FormController<CourseEntity> courseEntityFormController;

    @FXML
    private ProgressIndicator progressIndicator;

    private GroupEntity entity = null;
    @Override
    public void setEntity(GroupEntity entity) {

        this.entity = entity;
        if(entity != null){

            name.setText(entity.getNameGroup());

            teacherEntityFormController.setEntity(entity.getTeacherByTeacherId());
            courseEntityFormController.setEntity(entity.getCourseByIdCourse());
        } else {

            name.setText("");
        }
    }

    @Override
    public void initEntity() {
        if(isValid()){

            if(entity == null)
                entity = new GroupEntity();

            entity.setNameGroup(name.getText());
            entity.setTeacherByTeacherId(teacherEntityFormController.getEntity());
            entity.setCourseByIdCourse(courseEntityFormController.getEntity());
        }
    }

    @Override
    public boolean isValid() {

        String errorMessage = "";
        if(name.getText() == null || name.getText().length() < 1)
            errorMessage += "Названине  не заполнено!\n";

        if(teacherEntityFormController.getEntity() == null){

            errorMessage += "Нет преподавателя для группы!!!\n";
        }

        if(courseEntityFormController.getEntity() == null){

            errorMessage += "Нет выбранного курса для группы!!!\n";
        }

        if(errorMessage.length() == 0)
            return true;
        else
            DialogManager.showErrorDialog("Ошибка ввода данных",errorMessage);
        return false;
    }

    @Override
    public GroupEntity getEntity() {

        return entity;
    }

    @Override
    public void initialize() {

        try {

            teacherEntityFormController = FXLoaderController.loadFormController("TeacherView");

            teacherForm.getChildren().add(teacherEntityFormController.getLayout());

            courseEntityFormController = FXLoaderController.loadFormController("CourseView");

            courseForm.getChildren().add(courseEntityFormController.getLayout());

        } catch (Exception e) {
            DialogManager.showErrorDialog("Ошибка подгрузки данных", e.getMessage());
        }
    }

    @FXML
    private void onTeacher(){

        Repository.getResult(new ReadListTeacherCommand())
                .subscribe(new AbstractCommandShowError<TeacherEntity[]>() {
                    @Override
                    public void onNext(TeacherEntity[] teacherEntities) {

                        try {
                            ChoiceTableController<TeacherEntity> choiceTableController = (ChoiceTableController) FXLoaderController.loadTableController("ChoiceTableView");
                            TableController<TeacherEntity> courseEntityTableController = FXLoaderController.loadTableController("TeacherView");

                            choiceTableController.setController(courseEntityTableController);
                            Stage dialogStage = new Stage();

                            Scene scene = new Scene(choiceTableController.getLayout());
                            dialogStage.setScene(scene);
                            choiceTableController.setPrimaryStage(dialogStage);
                            choiceTableController.setList(ListUtil.parseArray(teacherEntities));

                            dialogStage.showAndWait();

                            if(choiceTableController.isOk()){

                                teacherEntityFormController.setEntity(choiceTableController.getSelect());
                            }
                        } catch (Exception ex){

                            DialogManager.showErrorDialog("Ошибка подгрузки данных", ex.getMessage());
                        }
                    }
                });

    }

    @FXML
    private void onCourse() {

        Repository.getResult(new ReadListCourseCommand())
                .subscribe(new AbstractCommandShowError<CourseEntity[]>() {
                    @Override
                    public void onNext(CourseEntity[] courseEntities) {

                        try {
                            ChoiceTableController<CourseEntity> choiceTableController = (ChoiceTableController) FXLoaderController.loadTableController("ChoiceTableView");
                            TableController<CourseEntity> courseEntityTableController = FXLoaderController.loadTableController("CourseView");

                            choiceTableController.setController(courseEntityTableController);
                            Stage dialogStage = new Stage();

                            Scene scene = new Scene(choiceTableController.getLayout());
                            dialogStage.setScene(scene);
                            choiceTableController.setPrimaryStage(dialogStage);
                            choiceTableController.setList(ListUtil.parseArray(courseEntities));

                            dialogStage.showAndWait();

                            if(choiceTableController.isOk()){

                                courseEntityFormController.setEntity(choiceTableController.getSelect());
                            }
                        } catch (Exception ex){

                            DialogManager.showErrorDialog("Ошибка подгрузки данных", ex.getMessage());
                        }
                    }
                });
    }

    @FXML
    private void onWorkTimeTable() {

        if(entity != null && entity.getIdGroup() > 0) {
            try {

                Stage stage = new Stage();

                NewWindowWorking newWindowWorking = (NewWindowWorking) FXLoaderController.loadController("NewWondowWorking");

                stage.setScene(new Scene(newWindowWorking.getLayout()));

                TableController<TimeTableEntity> tableController = FXLoaderController.loadTableController("TimeTableView");

                FormController<TimeTableEntity> formController = FXLoaderController.loadFormController("TimeTableView");

                BottomBarController bottomBarController = (BottomBarController) FXLoaderController.loadController("BottomBar");

                CreateEditDeleteBottomController createEditDeleteBottomController = (CreateEditDeleteBottomController) FXLoaderController.loadController("CreateEditDeleteBottomBarView");

                createEditDeleteBottomController.setCallback(new BottomBarTimeTable(entity.getIdGroup(), tableController, formController));

                bottomBarController.setAdminPane(createEditDeleteBottomController.getLayout());

                newWindowWorking.setBottomBarController(bottomBarController);
                newWindowWorking.setTableController(tableController);
                newWindowWorking.setFormController(formController);

                newWindowWorking.setPrimaryStage(stage);

                Repository.getTimeTable(tableController, entity.getIdGroup());

                tableController.setCallbackSelect(formController::setEntity);

                stage.show();

            } catch (IOException e) {
                DialogManager.showErrorDialog("Error", "Произошла ошибка " + e.getMessage());
            }
        } else {
            DialogManager.showErrorDialog("Error", "Произошла ошибка расписание можно создавать только для групп находящихся в базе!!!");
        }
    }

    @FXML
    private void onAddStudentTable() {

        if(entity != null && entity.getIdGroup() > 0) {

        Repository.getResult(new ReadListStudentCommand())
                .subscribe(new AbstractCommandShowError<StudentEntity[]>() {
                    @Override
                    public void onNext(StudentEntity[] studentEntities) {

                        try {
                            ChoiceTableController<StudentEntity> choiceTableController = (ChoiceTableController) FXLoaderController.loadTableController("ChoiceTableView");
                            TableController<StudentEntity> studentEntityTableController = FXLoaderController.loadTableController("StudentView");

                            choiceTableController.setController(studentEntityTableController);
                            Stage dialogStage = new Stage();

                            choiceTableController.setMultiSelect(true);

                            Scene scene = new Scene(choiceTableController.getLayout());
                            dialogStage.setScene(scene);
                            choiceTableController.setPrimaryStage(dialogStage);
                            choiceTableController.setList(ListUtil.parseArray(studentEntities));

                            dialogStage.showAndWait();

                            if(choiceTableController.isOk()){

                                List<StudentEntity> studentEntities1 = choiceTableController.getSelected();

                                List<Integer> integers = new ArrayList<>();

                                if(studentEntities1!=null){

                                    for(StudentEntity studentEntity: studentEntities1){

                                        integers.add(studentEntity.getIdStudent());
                                    }
                                }

                                StudentsIdByGroupEntity studentsIdByGroupEntity = new StudentsIdByGroupEntity();

                                studentsIdByGroupEntity.setIdGroup(entity.getIdGroup());
                                studentsIdByGroupEntity.setIdStudent(integers);

                                Repository.getResult(new AddStudentByGroup(studentsIdByGroupEntity))
                                        .subscribe(new AbstractCommandShowError<Boolean>() {
                                            @Override
                                            public void onNext(Boolean aBoolean) {

                                                DialogManager.showInfoDialog("Прошло успешно", "Добавление студентов в группу произошло успешно!");
                                            }
                                        });

                            }
                        } catch (Exception ex){

                            DialogManager.showErrorDialog("Ошибка подгрузки данных", ex.getMessage());
                        }
                    }
                });

        } else {
            DialogManager.showErrorDialog("Error", "Произошла ошибка студентов можно добавлять только для групп находящихся в базе!!!");
        }
    }

    @FXML
    private void showStudent() {

        if(entity != null && entity.getIdGroup() > 0) {
            try {

                Stage stage = new Stage();

                NewWindowWorking newWindowWorking = (NewWindowWorking) FXLoaderController.loadController("NewWondowWorking");

                stage.setScene(new Scene(newWindowWorking.getLayout()));

                TableController<StudentEntity> tableController = FXLoaderController.loadTableController("StudentView");

                newWindowWorking.setTableController(tableController);
                newWindowWorking.setPrimaryStage(stage);

                Repository.getStudents(tableController, entity.getIdGroup());

                stage.show();

            } catch (IOException e) {
                DialogManager.showErrorDialog("Error", "Произошла ошибка " + e.getMessage());
            }
        } else {
            DialogManager.showErrorDialog("Error", "Произошла ошибка расписание можно создавать только для групп находящихся в базе!!!");
        }
    }
    @Override
    public void displayIndicatorProgress() {
        dataPane.setVisible(false);
        progressIndicator.setVisible(true);
    }

    @Override
    public void closeIndicatorProgress() {
        dataPane.setVisible(true);
        progressIndicator.setVisible(false);
    }

    @Override
    public Pane getLayout() {
        return rootPane;
    }

    @Override
    public void setPrimaryStage(Stage primaryStage) {

    }
}
