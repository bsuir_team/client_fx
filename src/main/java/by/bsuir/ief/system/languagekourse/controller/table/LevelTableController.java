package by.bsuir.ief.system.languagekourse.controller.table;

import by.bsuir.ief.system.languagekourse.model.entity.LevelEntity;
import by.bsuir.ief.system.languagekourse.util.ListUtil;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.List;

public class LevelTableController implements TableController<LevelEntity> {

    private ObservableList<LevelEntity> entityObservableList = FXCollections.observableArrayList();
    private Stage primaryStage;
    private boolean isMultiSelect;
    private CallbackSelected<LevelEntity> callbackSelected = null;

    @FXML
    private AnchorPane rootPane;

    @FXML
    private ProgressIndicator progressIndicator;

    @FXML
    private TableView<LevelEntity> tableView;

    @FXML
    private TableColumn<LevelEntity, String> nameColumn;

    @Override
    public LevelEntity getSelect() {
        return tableView.getSelectionModel().getSelectedItem();
    }

    @Override
    public List<LevelEntity> getSelected() {
        return tableView.getSelectionModel().getSelectedItems();
    }

    @Override
    public void setList(List<LevelEntity> list) {
        entityObservableList = ListUtil.parseListToObserv(entityObservableList, list);

        initTable();

        if(entityObservableList.size() > 0){

            closeIndicatorProgress();
        } else {

            displayIndicatorProgress();
        }
    }

    private void initTable(){

        tableView.getItems().clear();

        nameColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));

        tableView.getSelectionModel().selectedItemProperty().
                addListener(((observable, oldValue, newValue) -> {
                    if(callbackSelected != null) callbackSelected.onSelectItem(newValue);
                }));

        if(isMultiSelect)
            tableView.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE
        );

        tableView.setItems(entityObservableList);
    }

    @Override
    public void setMultiSelect(boolean multiSelect) {
        isMultiSelect = multiSelect;

        initTable();
    }

    @Override
    public void setCallbackSelect(CallbackSelected<LevelEntity> callbackSelect) {
        callbackSelected = callbackSelect;
    }

    @Override
    public void initialize() {
        /*rootPane.setBackground(Style.getBackground());
        tableView.setBackground(Style.getBackground());*/
    }

    @Override
    public void displayIndicatorProgress() {
        tableView.setVisible(false);
        progressIndicator.setVisible(true);
    }

    @Override
    public void closeIndicatorProgress() {
        tableView.setVisible(true);
        progressIndicator.setVisible(false);
    }

    @Override
    public Pane getLayout() {
        return rootPane;
    }

    @Override
    public void setPrimaryStage(Stage primaryStage) {

    }
}
