package by.bsuir.ief.system.languagekourse.controller.bottombar;

import by.bsuir.ief.system.languagekourse.controller.CreateEditDeleteBottomController;
import by.bsuir.ief.system.languagekourse.controller.DialogManager;
import by.bsuir.ief.system.languagekourse.controller.form.FormController;
import by.bsuir.ief.system.languagekourse.controller.table.TableController;
import by.bsuir.ief.system.languagekourse.model.entity.LanguageEntity;
import by.bsuir.ief.system.languagekourse.model.storage.Repository;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommandShowError;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.language.AddLanguageCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.language.DeleteLanguageCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.language.UpdateLanguageCommand;

public class LanguageBottomBar implements CreateEditDeleteBottomController.CallbackBottomBarManipulation {

    private final TableController<LanguageEntity> tableController;
    private final FormController<LanguageEntity> formController;

    public LanguageBottomBar(TableController<LanguageEntity> tableController,
                             FormController<LanguageEntity> formController) {
        this.tableController = tableController;
        this.formController = formController;
    }

    @Override
    public void onCreate() {

        if (formController.isValid()) {
            formController.initEntity();
            Repository.getResult(new AddLanguageCommand(formController.getEntity()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                @Override
                public void onNext(Boolean aBoolean) {
                    DialogManager.showInfoDialog("Операция завершена успешно!", "Создание завершено успешно");
                    Repository.getLanguage(tableController);
                }
            });
        }
    }

    @Override
    public void onEdit() {
        if (formController.isValid()) {
            formController.initEntity();

            Repository.getResult(new UpdateLanguageCommand(formController.getEntity()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                @Override
                public void onNext(Boolean aBoolean) {

                    DialogManager.showInfoDialog("Операция завершена успешно!", "Редактирование завершено успешно");
                    Repository.getLanguage(tableController);
                }
            });
        }
    }

    @Override
    public void onDelete() {

        LanguageEntity entity = formController.getEntity();
        if(entity != null) {
            Repository.getResult(new DeleteLanguageCommand(formController.getEntity().getId()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                        @Override
                        public void onNext(Boolean aBoolean) {
                            DialogManager.showInfoDialog("Операция завершена успешно!", "Удаление завершено успешно");
                            Repository.getLanguage(tableController);
                        }
                    });
        } else {

            DialogManager.showWarningDialog("Ошибка!", " Не выбран элемент для удаления!!");
        }

    }
}
