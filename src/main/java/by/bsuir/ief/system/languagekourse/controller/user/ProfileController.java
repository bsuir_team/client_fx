package by.bsuir.ief.system.languagekourse.controller.user;

import by.bsuir.ief.system.languagekourse.FXLoaderController;
import by.bsuir.ief.system.languagekourse.controller.Controller;
import by.bsuir.ief.system.languagekourse.controller.DialogManager;
import by.bsuir.ief.system.languagekourse.controller.form.FormController;
import by.bsuir.ief.system.languagekourse.model.entity.StudentEntity;
import by.bsuir.ief.system.languagekourse.model.entity.UserEntity;
import by.bsuir.ief.system.languagekourse.model.storage.Repository;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommandShowError;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.student.GetSelfStudentCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.student.UpdateStudentCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.user.GetUserCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.user.UserUpdateCommand;
import javafx.fxml.FXML;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.Arrays;

public class ProfileController implements Controller {

    @FXML
    private BorderPane borderPane;

    private FormController<UserEntity> userEntityFormController;
    private FormController<StudentEntity> studentEntityFormController;

    @Override
    public void initialize() {

        try {

            userEntityFormController = FXLoaderController.loadFormController("UserEditFormView");
            studentEntityFormController = FXLoaderController.loadFormController("StudentView");

            borderPane.setLeft(userEntityFormController.getLayout());
            borderPane.setRight(studentEntityFormController.getLayout());

            Repository.getResult(new GetUserCommand())
                    .subscribe(new AbstractCommandShowError<UserEntity>() {
                        @Override
                        public void onNext(UserEntity entity) {
                            userEntityFormController.setEntity(entity);
                        }
                    });

            Repository.getResult(new GetSelfStudentCommand())
                    .subscribe(new AbstractCommandShowError<StudentEntity>() {
                        @Override
                        public void onNext(StudentEntity studentEntity) {
                            studentEntityFormController.setEntity(studentEntity);
                        }
                    });
        } catch (Exception e){

            DialogManager.showErrorDialog(
                    "Ошибка в файле",
                    "Непредвиденная ошибка!\n" + Arrays.toString(e.getStackTrace()));
        }
    }

    @Override
    public void displayIndicatorProgress() {

    }

    @Override
    public void closeIndicatorProgress() {

    }

    @FXML
    private void onUpdateUserInformation(){

        if(userEntityFormController.isValid()) {
            userEntityFormController.initEntity();

            Repository.getResult(new UserUpdateCommand(userEntityFormController.getEntity()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                        @Override
                        public void onNext(Boolean aBoolean) {
                            DialogManager.showInfoDialog("Операция завершена успешно!", "Обновление завершено успешно");
                        }
                    });

        }
    }

    @FXML
    private void onUpdateStudentInformation(){

        if(studentEntityFormController.isValid()) {
            studentEntityFormController.initEntity();

            Repository.getResult(new UpdateStudentCommand(studentEntityFormController.getEntity()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                        @Override
                        public void onNext(Boolean aBoolean) {
                            DialogManager.showInfoDialog("Операция завершена успешно!", "Обновление завершено успешно");
                        }
                    });
        }
    }

    @Override
    public Pane getLayout() {
        return borderPane;
    }

    @Override
    public void setPrimaryStage(Stage primaryStage) {
    }
}
