package by.bsuir.ief.system.languagekourse.model.storage;

import by.bsuir.ief.system.languagekourse.controller.table.TableController;
import by.bsuir.ief.system.languagekourse.model.entity.*;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommandShowError;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractObserver;
import by.bsuir.ief.system.languagekourse.model.storage.command.ICommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.course.ReadListCourseCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.group.ReadListGroupCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.language.ReadListLanguageCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.level.ReadListLevelCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.requeststudent.ReadListRequestStudentCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.student.ReadListStudentByGroupCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.student.ReadListStudentCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.teacher.ReadListTeacherCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.timetable.ReadListTimeTableByGroupCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.timetable.ReadListTimeTableCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.user.GetUserListCommand;
import by.bsuir.ief.system.languagekourse.util.ListUtil;
import com.sun.istack.internal.NotNull;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class Repository {

    public static <T>Observable<T> getResult(@NotNull final ICommand<T> command) {

        return command.execute();
    }

    public static void getTimeTable(TableController<TimeTableEntity> tableController, int idGroup){

        Repository.getResult(new ReadListTimeTableByGroupCommand(idGroup))
                .subscribe(getEntity(tableController));
    };

    public static void getGroup(TableController<GroupEntity> tableController){

        Repository.getResult(new ReadListGroupCommand()).subscribe(getEntity(tableController));
    }

    public static void getRequestStudents(TableController<RequestFullStudentEntity> tableController){
        Repository.getResult(new ReadListRequestStudentCommand())
                .subscribe(getEntity(tableController));
    }

    public static void getTimeTable(TableController<TimeTableEntity> tableController){

        Repository.getResult(new ReadListTimeTableCommand())
                .subscribe(getEntity(tableController));
    };
    public static void getCourse(TableController<CourseEntity> tableController){

        Repository.getResult(new ReadListCourseCommand())
                .subscribe(getEntity(tableController));
    }

    public static void getsUser(TableController<UserEntity> tableController) {

        Repository.getResult(new GetUserListCommand())
                .subscribe(getEntity(tableController));
    }

    public static void getsTeacher(TableController<TeacherEntity> tableController) {

        Repository.getResult(new ReadListTeacherCommand())
                .subscribe(getEntity(tableController));
    }

    public static void getStudents(TableController<StudentEntity> tableController){

        Repository.getResult(new ReadListStudentCommand())
                .subscribe(getEntity(tableController));
    }

    public static void getStudents(TableController<StudentEntity> tableController, int idGroup){

        Repository.getResult(new ReadListStudentByGroupCommand(idGroup))
                .subscribe(getEntity(tableController));
    }

    public static void getLanguage(TableController<LanguageEntity> tableController){

        Repository.getResult(new ReadListLanguageCommand())
                .subscribe(getEntity(tableController));
    }

    public static void getLevel(TableController<LevelEntity> tableController){

        Repository.getResult(new ReadListLevelCommand())
                .subscribe(getEntity(tableController));
    }

    private static <T> AbstractObserver<T[]> getEntity(TableController<T> tableController){

        return new AbstractCommandShowError<T[]>() {
            @Override
            public void onSubscribe(Disposable disposable) {
                super.onSubscribe(disposable);

                tableController.displayIndicatorProgress();
            }

            @Override
            public void onNext(T[] list) {

                tableController.setList(ListUtil.parseArray(list));
            }

            @Override
            public void onComplete() {

                super.onComplete();
                tableController.closeIndicatorProgress();
            }
        };
    }

}
