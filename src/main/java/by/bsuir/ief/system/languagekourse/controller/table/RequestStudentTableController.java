package by.bsuir.ief.system.languagekourse.controller.table;

import by.bsuir.ief.system.languagekourse.model.entity.RequestFullStudentEntity;
import by.bsuir.ief.system.languagekourse.util.ListUtil;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.List;

public class RequestStudentTableController implements TableController<RequestFullStudentEntity> {

    @FXML
    private AnchorPane rootView;
    @FXML
    private TableView<RequestFullStudentEntity> clientEntityTableView;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private TableColumn<RequestFullStudentEntity, String> nameCourse;
    @FXML
    private TableColumn<RequestFullStudentEntity, String> language;
    @FXML
    private TableColumn<RequestFullStudentEntity, String> level;
    @FXML
    private TableColumn<RequestFullStudentEntity, String> fioStudent;
    @FXML
    private TableColumn<RequestFullStudentEntity, String> isSuccess;


    private Stage primaryStage;
    private boolean isMultiSelect;

    private ObservableList<RequestFullStudentEntity> entityObservableList = FXCollections.observableArrayList();
    private CallbackSelected<RequestFullStudentEntity> callbackSelected = null;

    @Override
    public RequestFullStudentEntity getSelect() {
        return clientEntityTableView.getSelectionModel().getSelectedItem();
    }

    @Override
    public List<RequestFullStudentEntity> getSelected() {
        return clientEntityTableView.getSelectionModel().getSelectedItems();
    }

    @Override
    public void setList(List<RequestFullStudentEntity> list) {

        entityObservableList = ListUtil.parseListToObserv(entityObservableList, list);

        initTable();

        closeIndicatorProgress();

    }

    @Override
    public void setMultiSelect(boolean multiSelect) {

        isMultiSelect = multiSelect;

        initTable();
    }

    @Override
    public void setCallbackSelect(CallbackSelected<RequestFullStudentEntity> callbackSelect) {
        this.callbackSelected = callbackSelect;
    }

    @Override
    public void initialize() {

        /*rootView.setBackground(Style.getBackground());
        clientEntityTableView.setBackground(Style.getBackground());*/
    }

    @Override
    public void displayIndicatorProgress() {
        clientEntityTableView.setVisible(false);
        progressIndicator.setVisible(true);
    }

    @Override
    public void closeIndicatorProgress() {
        clientEntityTableView.setVisible(true);
        progressIndicator.setVisible(false);
    }

    @Override
    public Pane getLayout() {
        return rootView;
    }

    @Override
    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    private void initTable(){

        clientEntityTableView.getItems().clear();

        nameCourse.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCourseEntity().getName()));
        language.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCourseEntity().getLanguageByIdLanguage().getName()));
        level.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCourseEntity().getLevelByIdLevel().getName()));

        fioStudent.setCellValueFactory(cellData -> new SimpleStringProperty(
                (new StringBuilder().append(cellData.getValue().getStudentEntity().getSurname()).append(" ")
                        .append(cellData.getValue().getStudentEntity().getName()).append(" ")
                        .append(cellData.getValue().getStudentEntity().getPatronymic())
                        .toString())
        ));

        isSuccess.setCellValueFactory(cellData -> new SimpleStringProperty((cellData.getValue().getIsSuccessfullyRequest() > 0 ? "Обработана" : "Не обработана")));

        clientEntityTableView.getSelectionModel().selectedItemProperty().
                addListener(((observable, oldValue, newValue) -> {
                    if(callbackSelected != null) callbackSelected.onSelectItem(newValue);
                }));

        if(isMultiSelect)
            clientEntityTableView.getSelectionModel().setSelectionMode(
                    SelectionMode.MULTIPLE
            );

        clientEntityTableView.setItems(entityObservableList);
    }
}
