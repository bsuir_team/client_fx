package by.bsuir.ief.system.languagekourse.controller.form;

import by.bsuir.ief.system.languagekourse.controller.DialogManager;
import by.bsuir.ief.system.languagekourse.controller.factory.LanguageCallbackCombox;
import by.bsuir.ief.system.languagekourse.controller.factory.LanguageConverter;
import by.bsuir.ief.system.languagekourse.controller.valid.DoubleValid;
import by.bsuir.ief.system.languagekourse.controller.valid.ValidField;
import by.bsuir.ief.system.languagekourse.model.entity.CourseEntity;
import by.bsuir.ief.system.languagekourse.model.entity.LanguageEntity;
import by.bsuir.ief.system.languagekourse.model.entity.LevelEntity;
import by.bsuir.ief.system.languagekourse.model.storage.Repository;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommandShowError;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.language.ReadListLanguageCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.level.ReadListLevelCommand;
import by.bsuir.ief.system.languagekourse.util.ListUtil;
import by.bsuir.ief.system.languagekourse.util.Style;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;

import java.util.ArrayList;
import java.util.List;

public class CourseForm implements FormController<CourseEntity> {

    @FXML
    private Pane rootPane;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private GridPane contentView;

    @FXML
    private Label idLabel;
    @FXML
    private TextField name;
    @FXML
    private TextField priceTextField;
    @FXML
    private TextField hourLessonTextField;
    @FXML
    private TextArea descriptionTextField;

    @FXML
    private ComboBox<LevelEntity> levelEntityComboBox;
    private ObservableList<LevelEntity> levelEntityObservableList = FXCollections.observableArrayList();

    @FXML
    private ComboBox<LanguageEntity> languageEntityComboBox;
    private ObservableList<LanguageEntity> languageEntityObservableList = FXCollections.observableArrayList();


    private CourseEntity entity = null;
    private ValidField<String> doubleValid = new DoubleValid();

    @Override
    public void initialize() {

       /* rootPane.setBackground(Style.getBackground());
        contentView.setBackground(Style.getBackground());
*/
        initLevel(new ArrayList<>());
        initLanguage(new ArrayList<>());

        Repository.getResult(new ReadListLevelCommand())
                .subscribe(new AbstractCommandShowError<LevelEntity[]>() {
                    @Override
                    public void onNext(LevelEntity[] typeFirmaEntities) {
                        initLevel(ListUtil.parseArray(typeFirmaEntities));
                    }
                });

        Repository.getResult(new ReadListLanguageCommand())
                .subscribe(new AbstractCommandShowError<LanguageEntity[]>() {
                    @Override
                    public void onNext(LanguageEntity[] typeFirmaEntities) {
                        initLanguage(ListUtil.parseArray(typeFirmaEntities));
                    }
                });

        idLabel.setText("0");
    }

    @Override
    public void setEntity(CourseEntity entity) {

        this.entity = entity;
        if(entity != null){

            idLabel.setText(String.valueOf(entity.getId()));
            name.setText(entity.getName());
            priceTextField.setText(String.valueOf(entity.getPrice()));
            hourLessonTextField.setText(String.valueOf(entity.getHoursLesson()));
            levelEntityComboBox.setValue(entity.getLevelByIdLevel());
            languageEntityComboBox.setValue(entity.getLanguageByIdLanguage());
            descriptionTextField.setText(String.valueOf(entity.getDescription()));


        } else {

            idLabel.setText("0");
            name.setText("");
            priceTextField.setText("");

        }
    }

    @Override
    public void initEntity() {
        if(isValid()){

            if(entity == null)
                entity = new CourseEntity();

            entity.setName(name.getText());
            entity.setPrice(Double.parseDouble(priceTextField.getText()));
            entity.setId(Integer.parseInt(idLabel.getText()));
            entity.setHoursLesson(Integer.valueOf(hourLessonTextField.getText()));
            entity.setLevelByIdLevel(levelEntityComboBox.getValue());
            entity.setLanguageByIdLanguage(languageEntityComboBox.getValue());
            entity.setDescription(descriptionTextField.getText());
        }
    }

    @Override
    public boolean isValid() {

        String errorMessage = "";
        //Required field

        if (name.getText().length() < 1){
            errorMessage += "Поле \"Название\" обязательно к заполнению\n";
            name.setStyle(Style.getErrorStyle());
        } else {
            name.setStyle(null);
        }

        if(hourLessonTextField.getText().length() < 1) {
            errorMessage += "Поле \"Колличество часов\" обязательно к заполнению\n";
            hourLessonTextField.setStyle(Style.getErrorStyle());
        } else if(!doubleValid.isValid(hourLessonTextField.getText())) {
            errorMessage += "Ошибка ввода \"Колличество часов\"\n";
            hourLessonTextField.setStyle(Style.getErrorStyle());
        } else {
            hourLessonTextField.setStyle(null);
        }

        if(descriptionTextField.getText().length() < 1) {
            errorMessage += "Поле \"Подробное описание\" обязательно к заполнению\n";
            descriptionTextField.setStyle(Style.getErrorStyle());
        } else {
            descriptionTextField.setStyle(null);
        }

        if(priceTextField.getText().length() < 1) {
            errorMessage += "Поле \"Цена\" обязательно к заполнению\n";
            priceTextField.setStyle(Style.getErrorStyle());
        } else if(!doubleValid.isValid(priceTextField.getText())) {
            errorMessage += "Ошибка ввода \"Цена\"\n";
            priceTextField.setStyle(Style.getErrorStyle());
        } else {
            priceTextField.setStyle(null);
        }

        if(languageEntityComboBox.getValue() == null) {

            errorMessage += "Поле \"Язык\" обязательно к заполнению\n";
            languageEntityComboBox.setStyle(Style.getErrorStyle());
        } else {
            languageEntityComboBox.setStyle(null);
        }

        if(levelEntityComboBox.getValue() == null) {

            errorMessage += "Поле \"Уровень\" обязательно к заполнению\n";
            levelEntityComboBox.setStyle(Style.getErrorStyle());
        } else {
            levelEntityComboBox.setStyle(null);
        }

        if(errorMessage.length() == 0)
            return true;
        else
            DialogManager.showErrorDialog("Ошибка ввода данных",errorMessage);
        return false;
    }

    /**
     * Вызывать только после метода {@link #initEntity()}
     * @return
     */
    @Override
    public CourseEntity getEntity() {

        return entity;
    }


    @Override
    public void displayIndicatorProgress() {
        contentView.setVisible(false);
        progressIndicator.setVisible(true);
    }

    @Override
    public void closeIndicatorProgress() {
        contentView.setVisible(true);
        progressIndicator.setVisible(false);
    }

    @Override
    public Pane getLayout() {
        return rootPane;
    }

    @Override
    public void setPrimaryStage(Stage primaryStage) {

    }

    private void initLanguage(List<LanguageEntity> entities){
        languageEntityObservableList = ListUtil.parseListToObserv(languageEntityObservableList, entities);

        languageEntityComboBox.setCellFactory(new LanguageCallbackCombox());
        languageEntityComboBox.setConverter(new LanguageConverter());

        languageEntityComboBox.setItems(languageEntityObservableList);

        if(entity != null){

            languageEntityComboBox.setValue(entity.getLanguageByIdLanguage());
        }
    }

    private void initLevel(List<LevelEntity> entities) {

        levelEntityObservableList = ListUtil.parseListToObserv(levelEntityObservableList, entities);

        levelEntityComboBox.setCellFactory(new Callback<ListView<LevelEntity>, ListCell<LevelEntity>>() {
            @Override
            public ListCell<LevelEntity> call(ListView<LevelEntity> levelEntityListView) {
                final ListCell<LevelEntity> cell = new ListCell<LevelEntity>() {

                    @Override
                    protected void updateItem(LevelEntity t, boolean bln) {
                        super.updateItem(t, bln);

                        if (t != null) {
                            setText(t.getName());
                        } else {
                            setText(null);
                        }
                    }
                };

                return cell;
            }
        });
        levelEntityComboBox.setConverter(new StringConverter<LevelEntity>() {
            @Override
            public String toString(LevelEntity levelEntity) {
                return levelEntity.getName();
            }

            @Override
            public LevelEntity fromString(String s) {
                return null;
            }
        });

        levelEntityComboBox.setItems(levelEntityObservableList);

        if(entity != null){

            levelEntityComboBox.setValue(entity.getLevelByIdLevel());
        }
    }

}
