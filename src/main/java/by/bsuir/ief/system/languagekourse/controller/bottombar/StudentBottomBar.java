package by.bsuir.ief.system.languagekourse.controller.bottombar;

import by.bsuir.ief.system.languagekourse.controller.CreateEditDeleteBottomController;
import by.bsuir.ief.system.languagekourse.controller.DialogManager;
import by.bsuir.ief.system.languagekourse.controller.form.FormController;
import by.bsuir.ief.system.languagekourse.controller.table.TableController;
import by.bsuir.ief.system.languagekourse.model.entity.StudentEntity;
import by.bsuir.ief.system.languagekourse.model.storage.Repository;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommandShowError;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.student.AddStudentCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.student.DeleteStudentCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.student.UpdateStudentCommand;

public class StudentBottomBar implements CreateEditDeleteBottomController.CallbackBottomBarManipulation {

    private final TableController<StudentEntity> tableController;
    private final FormController<StudentEntity> formController;

    public StudentBottomBar(TableController<StudentEntity> tableController,
                            FormController<StudentEntity> formController) {
        this.tableController = tableController;
        this.formController = formController;
    }

    @Override
    public void onCreate() {

        if (formController.isValid()) {
            formController.initEntity();
            Repository.getResult(new AddStudentCommand(formController.getEntity()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                @Override
                public void onNext(Boolean aBoolean) {
                    DialogManager.showInfoDialog("Операция завершена успешно!", "Создание завершено успешно");
                    Repository.getStudents(tableController);
                }
            });
        }
    }

    @Override
    public void onEdit() {
        if (formController.isValid()) {
            formController.initEntity();

            Repository.getResult(new UpdateStudentCommand(formController.getEntity()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                @Override
                public void onNext(Boolean aBoolean) {

                    DialogManager.showInfoDialog("Операция завершена успешно!", "Редактирование завершено успешно");
                    Repository.getStudents(tableController);
                }
            });
        }
    }

    @Override
    public void onDelete() {

        StudentEntity entity = formController.getEntity();
        if(entity != null) {

            Repository.getResult(new DeleteStudentCommand(formController.getEntity().getIdStudent()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                        @Override
                        public void onNext(Boolean aBoolean) {
                            DialogManager.showInfoDialog("Операция завершена успешно!", "Удаление завершено успешно");
                            Repository.getStudents(tableController);
                        }
                    });
        } else {

            DialogManager.showWarningDialog("Ошибка!", " Не выбран элемент для удаления!!");
        }

    }
}
