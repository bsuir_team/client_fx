package by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.timetable;


import by.bsuir.ief.system.languagekourse.model.entity.TimeTableEntity;
import by.bsuir.ief.system.languagekourse.model.storage.ConnectToServer;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommand;
import com.google.gson.GsonBuilder;
import connection.client.Request;
import connection.client.requestbody.NoContentRequestBody;
import io.reactivex.Observable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;

public class ReadListTimeTableCommand extends AbstractCommand<TimeTableEntity[]> {

    @Override
    protected Request getRequest() throws Exception {
        Request.Builder builder = new Request.Builder()
                .body(new NoContentRequestBody())
                .addMethod("readListTimeTable");

        builder = ConnectToServer.setConnectData(builder);

        return builder.build();
    }

    @Override
    public Observable<TimeTableEntity[]> execute() {
        return super.getObservable()
                .map(response -> new GsonBuilder().setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz")
                        .create().fromJson(new String(response.getBody().bytes()), TimeTableEntity[].class))
                .observeOn(JavaFxScheduler.platform());
    }
}
