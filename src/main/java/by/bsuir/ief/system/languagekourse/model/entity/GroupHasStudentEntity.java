package by.bsuir.ief.system.languagekourse.model.entity;

import java.util.Objects;

public class GroupHasStudentEntity {
    private int groupIdGroup;
    private int studentIdStudent;

    public int getGroupIdGroup() {
        return groupIdGroup;
    }

    public void setGroupIdGroup(int groupIdGroup) {
        this.groupIdGroup = groupIdGroup;
    }

    public int getStudentIdStudent() {
        return studentIdStudent;
    }

    public void setStudentIdStudent(int studentIdStudent) {
        this.studentIdStudent = studentIdStudent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupHasStudentEntity that = (GroupHasStudentEntity) o;
        return groupIdGroup == that.groupIdGroup &&
                studentIdStudent == that.studentIdStudent;
    }

    @Override
    public int hashCode() {

        return Objects.hash(groupIdGroup, studentIdStudent);
    }
}
