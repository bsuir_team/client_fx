package by.bsuir.ief.system.languagekourse.controller;

import by.bsuir.ief.system.languagekourse.FXLoaderController;
import by.bsuir.ief.system.languagekourse.controller.form.FormController;
import by.bsuir.ief.system.languagekourse.controller.table.TableController;
import by.bsuir.ief.system.languagekourse.controller.user.ProfileController;
import by.bsuir.ief.system.languagekourse.model.entity.CourseEntity;
import by.bsuir.ief.system.languagekourse.model.entity.LevelSumStudentEntity;
import by.bsuir.ief.system.languagekourse.model.entity.RequestStudentEntity;
import by.bsuir.ief.system.languagekourse.model.entity.TimeTableEntity;
import by.bsuir.ief.system.languagekourse.model.storage.Repository;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommandShowError;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractObserver;
import by.bsuir.ief.system.languagekourse.model.storage.command.ExitCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.level.ReadLevelSumStudentCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.requeststudent.AddRequestStudentByCourse;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.timetable.ReadMyTimeTableCommand;
import by.bsuir.ief.system.languagekourse.util.ListUtil;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class UserRoomController implements Controller {

    private Stage primaryStage;
    @FXML
    private BorderPane pane;

    @FXML
    private AnchorPane centerPane;

    @FXML
    @Override
    public void initialize() {

       /* pane.setBackground(Style.getBackground());
        centerPane.setBackground(Style.getBackground());*/
    }

    @Override
    public void displayIndicatorProgress() {

    }

    @Override
    public void closeIndicatorProgress() {

    }

    @Override
    public Pane getLayout() {
        return pane;
    }

    @Override
    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    @FXML
    private void handleProfile(){

        clearPane();

        try {
            ProfileController profileController = (ProfileController) FXLoaderController.loadController("ProfileView");

            pane.setCenter(profileController.getLayout());

        } catch (IOException e) {
            DialogManager.showErrorDialog("Error", "Произошла ошибка " + e.getMessage());
        }
    }

    @FXML
    private void handleShowCourse(){

        try {
            TableController<CourseEntity> tableController = FXLoaderController.loadTableController("CourseView");

            FormController<CourseEntity> formController = FXLoaderController.loadFormController("CourseView");

            BottomBarController bottomBarController = (BottomBarController) FXLoaderController.loadController("BottomBar");

            ButtonView buttonView = (ButtonView) FXLoaderController.loadController("ButtonView");

            bottomBarController.setAdminPane(buttonView.getLayout());

            buttonView.setOnClick(() -> {

                if(tableController.getSelect() != null){

                    RequestStudentEntity requestStudentEntity = new RequestStudentEntity();
                    requestStudentEntity.setIdStudent(0);
                    requestStudentEntity.setIdCourse(tableController.getSelect().getId());
                    requestStudentEntity.setIsSuccessfullyRequest(-1);

                    Repository.getResult(new AddRequestStudentByCourse(requestStudentEntity))
                            .subscribe(new AbstractCommandShowError<Boolean>() {
                                @Override
                                public void onNext(Boolean aBoolean) {
                                    DialogManager.showInfoDialog("Прошло успешно",
                                            "Ваша заявка успешно была добавлена на рассмотрение администратром");
                                }
                            });

                } else {
                    DialogManager.showWarningDialog("Вы не выбрали курс!",
                            "Для записи на курсы необходимо выбрать определенный курс " +
                            "\n и потом на него можно записаться!!!!");
                }
            });

            pane.setBottom(bottomBarController.getLayout());

            Repository.getCourse(tableController);

            tableController.setCallbackSelect(formController::setEntity);

            pane.setCenter(tableController.getLayout());
            pane.setLeft(formController.getLayout());

        } catch (IOException e) {
            DialogManager.showErrorDialog("Error", "Произошла ошибка " + e.getMessage());
        }
    }

    @FXML
    private void handleTableTime(){

        clearPane();

        try {

            TableController<TimeTableEntity> tableController = FXLoaderController.loadTableController("TimeTableView");

            FormController<TimeTableEntity> formController = FXLoaderController.loadFormController("TimeTableView");

            Repository.getResult(new ReadMyTimeTableCommand())
                    .subscribe(new AbstractCommandShowError<TimeTableEntity[]>() {
                        @Override
                        public void onNext(TimeTableEntity[] timeTableEntities) {

                            tableController.setList(ListUtil.parseArray(timeTableEntities));
                        }
                    });

            tableController.setCallbackSelect(formController::setEntity);

            pane.setCenter(tableController.getLayout());
            pane.setLeft(formController.getLayout());

        } catch (IOException e) {
            DialogManager.showErrorDialog("Error", "Произошла ошибка " + e.getMessage());
        }
    }

    @FXML
    private void handleStatiscitcs(){

            Repository.getResult(new ReadLevelSumStudentCommand())
                    .subscribe(new AbstractCommandShowError<LevelSumStudentEntity[]>() {
                        @Override
                        public void onNext(LevelSumStudentEntity[] levelSumStudentEntities) {

                            try {
                                FXMLLoader loader = FXLoaderController.load("LevelSumStudentStatistics");

                                LevelSumStudentStatisticsController controller = loader.getController();
                                Stage dialogStage = new Stage();
                                dialogStage.setTitle("Статистика по количеству студентов на уровнях языковых курсов");
                                dialogStage.initModality(Modality.WINDOW_MODAL);
                                dialogStage.initOwner(primaryStage);
                                Scene scene = new Scene(controller.getRootPane());
                                dialogStage.setScene(scene);

                                controller.setLevelSumStudentData(ListUtil.parseArray(levelSumStudentEntities));
                                dialogStage.show();

                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    });
    }

    private void clearPane(){

        pane.setLeft(null);
        pane.setRight(null);
        pane.setCenter(null);
        pane.setBottom(null);
    }

    @FXML
    private void handleExit(){
        try {
            Repository.getResult(new ExitCommand()).subscribe(new AbstractObserver<Boolean>() {
                @Override
                public void onNext(Boolean aBoolean) {

                }

                @Override
                public void onError(Throwable throwable) {

                }
            });

            FXMLLoader loader = FXLoaderController.load("ConnectionView");

            ConnectionViewController controller = loader.getController();

            Scene scene = new Scene(controller.getRootLayout());
            primaryStage.setScene(scene);

            controller.setPrimaryStage(this.primaryStage);
            primaryStage.show();

        } catch (IOException e) {
            DialogManager.showErrorDialog(
                    "Ошибка в файле",
                    "Непредвиденная ошибка!");
        }
    }

}
