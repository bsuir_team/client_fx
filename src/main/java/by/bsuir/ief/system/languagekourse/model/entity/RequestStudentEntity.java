package by.bsuir.ief.system.languagekourse.model.entity;

import java.util.Objects;

public class RequestStudentEntity {

    private int idRequestStudent;
    private int idStudent;
    private int idCourse;
    private int isSuccessfullyRequest;


    public int getIdRequestStudent() {
        return idRequestStudent;
    }

    public void setIdRequestStudent(int idRequestStudent) {
        this.idRequestStudent = idRequestStudent;
    }

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public int getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(int idCourse) {
        this.idCourse = idCourse;
    }

    public int getIsSuccessfullyRequest() {
        return isSuccessfullyRequest;
    }

    public void setIsSuccessfullyRequest(int isSuccessfullyRequest) {
        this.isSuccessfullyRequest = isSuccessfullyRequest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RequestStudentEntity that = (RequestStudentEntity) o;
        return idRequestStudent == that.idRequestStudent &&
                idStudent == that.idStudent &&
                idCourse == that.idCourse &&
                isSuccessfullyRequest == that.isSuccessfullyRequest;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idRequestStudent, idStudent, idCourse, isSuccessfullyRequest);
    }
}
