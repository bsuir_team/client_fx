package by.bsuir.ief.system.languagekourse.controller.valid;

public interface ValidField<T> {

    boolean isValid(T value);
}
