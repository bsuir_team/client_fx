package by.bsuir.ief.system.languagekourse.controller.factory;

import by.bsuir.ief.system.languagekourse.model.entity.LanguageEntity;
import javafx.util.StringConverter;

public class LanguageConverter extends StringConverter<LanguageEntity> {
    @Override
    public String toString(LanguageEntity t) {
        return t.getName();
    }

    @Override
    public LanguageEntity fromString(String string) {
        throw new RuntimeException("No implement exception");
    }
}
