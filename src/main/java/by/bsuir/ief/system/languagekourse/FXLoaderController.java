package by.bsuir.ief.system.languagekourse;

import by.bsuir.ief.system.languagekourse.controller.Controller;
import by.bsuir.ief.system.languagekourse.controller.EditController;
import by.bsuir.ief.system.languagekourse.controller.form.FormController;
import by.bsuir.ief.system.languagekourse.controller.table.TableController;
import javafx.fxml.FXMLLoader;

import java.io.IOException;

public class FXLoaderController {

    public static Controller loadController(final String nameViewFxml) throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(FXLoaderController.class.getResource("/view/"+ nameViewFxml +".fxml"));

        loader.load();
        return loader.getController();
    }

    public static <T> EditController<T> loadEditController(final String nameViewEditFxml) throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(FXLoaderController.class.getResource("/view/"+ nameViewEditFxml +".fxml"));

        loader.load();
        return loader.getController();
    }

    public static <T> FormController<T> loadFormController(final String nameViewFormFxml) throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(FXLoaderController.class.getResource("/view/form/"+ nameViewFormFxml +".fxml"));

        loader.load();
        return loader.getController();
    }

    public static <T>TableController<T> loadTableController(final String nameTableFxml) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(FXLoaderController.class.getResource("/view/table/"+ nameTableFxml +".fxml"));

        loader.load();
        return loader.getController();
    }

    public static FXMLLoader load(final String nameViewFxml) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(FXLoaderController.class.getResource("/view/" + nameViewFxml +".fxml"));
        loader.load();

        return loader;
    }
}
