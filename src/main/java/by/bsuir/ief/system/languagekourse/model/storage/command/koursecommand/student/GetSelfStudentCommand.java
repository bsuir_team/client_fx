package by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.student;

import by.bsuir.ief.system.languagekourse.model.entity.StudentEntity;
import by.bsuir.ief.system.languagekourse.model.storage.ConnectToServer;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommand;
import com.google.gson.GsonBuilder;
import connection.client.Request;
import connection.client.requestbody.NoContentRequestBody;
import io.reactivex.Observable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;

public class GetSelfStudentCommand extends AbstractCommand<StudentEntity> {

    @Override
    protected Request getRequest() throws Exception {
        Request.Builder builder = new Request.Builder()
                .body(new NoContentRequestBody())
                .addMethod("readCurrentStudent");

        builder = ConnectToServer.setConnectData(builder);

        return builder.build();
    }

    @Override
    public Observable<StudentEntity> execute() {
        return super.getObservable()
                .map(response -> new GsonBuilder().setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz").create().fromJson(new String(response.getBody().bytes()), StudentEntity.class))
                .observeOn(JavaFxScheduler.platform());
    }
}
