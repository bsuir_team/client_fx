package by.bsuir.ief.system.languagekourse.model.entity;

import java.util.List;

public class StudentsIdByGroupEntity {

    private int idGroup;

    private List<Integer> idStudent;

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public List<Integer> getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(List<Integer> idStudent) {
        this.idStudent = idStudent;
    }
}
