package by.bsuir.ief.system.languagekourse.controller.form;

import by.bsuir.ief.system.languagekourse.controller.DialogManager;
import by.bsuir.ief.system.languagekourse.controller.factory.LanguageCallbackCombox;
import by.bsuir.ief.system.languagekourse.controller.factory.LanguageConverter;
import by.bsuir.ief.system.languagekourse.controller.valid.TextFieldValid;
import by.bsuir.ief.system.languagekourse.controller.valid.ValidField;
import by.bsuir.ief.system.languagekourse.model.entity.LanguageEntity;
import by.bsuir.ief.system.languagekourse.model.entity.TeacherEntity;
import by.bsuir.ief.system.languagekourse.model.storage.Repository;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommandShowError;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.language.ReadListLanguageCommand;
import by.bsuir.ief.system.languagekourse.util.ListUtil;
import by.bsuir.ief.system.languagekourse.util.Style;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class TeacherFormController implements FormController<TeacherEntity> {

    @FXML
    private Pane rootPane;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private GridPane contentView;
    @FXML
    private TextField surnameRequired;
    @FXML
    private TextField nameRequired;
    @FXML
    private TextField patronymicRequired;

    @FXML
    private ComboBox<LanguageEntity> languageEntityComboBox;
    private ObservableList<LanguageEntity> languageEntityObservableList = FXCollections.observableArrayList();

    private TeacherEntity entity = null;
    private ValidField<String> nameValid = new TextFieldValid();

    @Override
    public void initialize() {

        /*rootPane.setBackground(Style.getBackground());
        contentView.setBackground(Style.getBackground());
*/
        initLanguageComboBox(new ArrayList<>());

        Repository.getResult(new ReadListLanguageCommand())
                .subscribe(new AbstractCommandShowError<LanguageEntity[]>() {
            @Override
            public void onNext(LanguageEntity[] languageEntities) {
                initLanguageComboBox(ListUtil.parseArray(languageEntities));
            }
        });
    }

    @Override
    public void setEntity(TeacherEntity entity) {

        this.entity = entity;
        if(entity != null){

            surnameRequired.setText(entity.getSurname());
            nameRequired.setText(entity.getName());
            patronymicRequired.setText(entity.getPatronymic());

            languageEntityComboBox.setValue(entity.getLanguageByIdLanguage());

        }
    }

    @Override
    public void initEntity() {
        if(isValid()){

            if(entity == null)
                entity = new TeacherEntity();

            entity.setSurname(surnameRequired.getText());
            entity.setName(nameRequired.getText());
            entity.setPatronymic(patronymicRequired.getText());

            entity.setLanguageByIdLanguage(languageEntityComboBox.getValue());
        }
    }

    @Override
    public boolean isValid() {

        String errorMessage = "";
        //Required field

        if (surnameRequired.getText().length() < 1){
            errorMessage += "Поле \"Фамилия\" обязательно к заполнению\n";
            surnameRequired.setStyle(Style.getErrorStyle());
        } else if(!nameValid.isValid(surnameRequired.getText())) {
            errorMessage += "Ошибка при вводе Фамилии\n";
            surnameRequired.setStyle(Style.getErrorStyle());
        } else {
            surnameRequired.setStyle(null);
        }

        if(nameRequired.getText().length() < 1) {
            errorMessage += "Поле \"Имя\" обязательно к заполнению\n";
            nameRequired.setStyle(Style.getErrorStyle());
        } else if(!nameValid.isValid(nameRequired.getText())) {
            errorMessage += "Ошибка ввода Имени\n";
            nameRequired.setStyle(Style.getErrorStyle());
        } else {
            nameRequired.setStyle(null);
        }

        if(patronymicRequired.getText().length() < 1) {
            errorMessage += "Поле \"Отчество\" обязательно к заполнению\n";
            patronymicRequired.setStyle(Style.getErrorStyle());
        } else if(!nameValid.isValid(patronymicRequired.getText())) {
            errorMessage += "Ошибка ввода Отчества\n";
            patronymicRequired.setStyle(Style.getErrorStyle());
        } else {
            patronymicRequired.setStyle(null);
        }

        if(languageEntityComboBox.getValue() == null) {

            errorMessage += "Поле \"Язык\" обязательно к заполнению\n";
            languageEntityComboBox.setStyle(Style.getErrorStyle());
        } else {
            languageEntityComboBox.setStyle(null);
        }

        if(errorMessage.length() == 0)
            return true;
        else
            DialogManager.showErrorDialog("Ошибка ввода данных",errorMessage);
        return false;
    }

    /**
     * Вызывать только после метода {@link #initEntity()}
     * @return
     */
    @Override
    public TeacherEntity getEntity() {

        return entity;
    }


    @Override
    public void displayIndicatorProgress() {
        contentView.setVisible(false);
        progressIndicator.setVisible(true);
    }

    @Override
    public void closeIndicatorProgress() {
        contentView.setVisible(true);
        progressIndicator.setVisible(false);
    }

    @Override
    public Pane getLayout() {
        return rootPane;
    }

    @Override
    public void setPrimaryStage(Stage primaryStage) {

    }

    private void initLanguageComboBox(List<LanguageEntity> nationalityEntities) {

        languageEntityObservableList = ListUtil.parseListToObserv(languageEntityObservableList, nationalityEntities);

        languageEntityComboBox.setCellFactory(new LanguageCallbackCombox());
        languageEntityComboBox.setConverter(new LanguageConverter());

        languageEntityComboBox.setItems(languageEntityObservableList);
    }

}
