package by.bsuir.ief.system.languagekourse.controller.form;


import by.bsuir.ief.system.languagekourse.controller.Controller;

public interface FormController<T> extends Controller {

    void setEntity(T entity);
    void initEntity();
    boolean isValid();
    T getEntity();
}
