package by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.teacher;


import by.bsuir.ief.system.languagekourse.model.entity.TeacherEntity;
import by.bsuir.ief.system.languagekourse.model.storage.ConnectToServer;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommand;
import com.sun.istack.internal.NotNull;
import connection.client.Request;
import connection.client.requestbody.JsonRequestBody;
import io.reactivex.Observable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;

public class AddTeacherCommand extends AbstractCommand<Boolean> {

    @NotNull
    private final TeacherEntity entity;

    public AddTeacherCommand(@NotNull final TeacherEntity entity) {
        this.entity = entity;
    }

    @Override
    protected Request getRequest() throws Exception {
        Request.Builder builder = new Request.Builder()
                .body(new JsonRequestBody(entity))
                    .addMethod("addTeacher");

        builder = ConnectToServer.setConnectData(builder);

        return builder.build();
    }

    @Override
    public Observable<Boolean> execute() {
        return super.getObservable()
                .map(response -> response.isSuccessfully())
                .observeOn(JavaFxScheduler.platform());
    }
}
