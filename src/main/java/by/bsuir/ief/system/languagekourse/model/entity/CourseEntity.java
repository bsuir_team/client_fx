package by.bsuir.ief.system.languagekourse.model.entity;

import java.util.Objects;

public class CourseEntity {
    private int id;
    private String name;
    private double price;
    private int hoursLesson;
    private String description;
    private LanguageEntity languageByIdLanguage;
    private LevelEntity levelByIdLevel;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getHoursLesson() {
        return hoursLesson;
    }

    public void setHoursLesson(int hoursLesson) {
        this.hoursLesson = hoursLesson;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CourseEntity that = (CourseEntity) o;
        return id == that.id &&
                Double.compare(that.price, price) == 0 &&
                hoursLesson == that.hoursLesson &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, price, hoursLesson, description);
    }

    public LanguageEntity getLanguageByIdLanguage() {
        return languageByIdLanguage;
    }

    public void setLanguageByIdLanguage(LanguageEntity languageByIdLanguage) {
        this.languageByIdLanguage = languageByIdLanguage;
    }

    public LevelEntity getLevelByIdLevel() {
        return levelByIdLevel;
    }

    public void setLevelByIdLevel(LevelEntity levelByIdLevel) {
        this.levelByIdLevel = levelByIdLevel;
    }
}
