package by.bsuir.ief.system.languagekourse.controller.form;

import by.bsuir.ief.system.languagekourse.FXLoaderController;
import by.bsuir.ief.system.languagekourse.controller.DialogManager;
import by.bsuir.ief.system.languagekourse.controller.EditController;
import by.bsuir.ief.system.languagekourse.model.entity.RoleApplicationEntity;
import by.bsuir.ief.system.languagekourse.model.entity.StudentEntity;
import by.bsuir.ief.system.languagekourse.model.entity.UserEntity;
import by.bsuir.ief.system.languagekourse.model.storage.Repository;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommandShowError;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractObserver;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.student.AddStudentCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.student.ReadStudentByIdUserCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.student.UpdateStudentCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.user.RoleApplicationListCommand;
import by.bsuir.ief.system.languagekourse.util.Crypto;
import by.bsuir.ief.system.languagekourse.util.ListUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;

import java.util.Arrays;

public class UserFomController implements FormController<UserEntity> {

    @FXML
    private Pane rootPane;

    @FXML
    private Pane dataPane;

    @FXML
    private ProgressIndicator progressIndicator;

    @FXML
    private Label idLabel;

    @FXML
    private TextField loginField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private ComboBox<RoleApplicationEntity> roleApp;

    @FXML
    private CheckBox statusUser;

    @FXML
    private Label infoBlockStatusUser;

    ObservableList<RoleApplicationEntity> typeEntities = FXCollections.observableArrayList();
    private UserEntity entity = null;

    @FXML
    private void handleClear(){
        clearField();
    }

    @FXML
    private void addStudent(){

        if(entity != null && entity.getIdUser() > 0) {

            Repository.getResult(new ReadStudentByIdUserCommand(entity.getIdUser()))
                    .subscribe(new AbstractObserver<StudentEntity>() {
                        @Override
                        public void onNext(StudentEntity studentEntity) {
                            openStageCreateStudent(studentEntity, false);
                        }

                        @Override
                        public void onError(Throwable throwable) {

                                StudentEntity studentEntity = new StudentEntity();
                            studentEntity.setIdUser(entity.getIdUser());

                            openStageCreateStudent(studentEntity, true);
                        }
                    });

        } else {

            DialogManager.showErrorDialog("Error", "Для этого пользователя нельзя создать студента");
        }
    }

    private void openStageCreateStudent(final StudentEntity studentEntity, boolean isCreate) {
        try {

            Stage stage = new Stage();

            FormController<StudentEntity> entityFormController = FXLoaderController.loadFormController("StudentView");

            EditController<StudentEntity> entityEditController = FXLoaderController.loadEditController("EditView");

            entityEditController.setFormController(entityFormController);

            entityEditController.setEntity(studentEntity);

            stage.setScene(new Scene(entityEditController.getLayout()));

            entityEditController.setPrimaryStage(stage);

            stage.showAndWait();

            if (entityEditController.isOk()) {

                entityEditController.init();

                if(isCreate) {
                    Repository.getResult(new AddStudentCommand(entityEditController.getEntity()))
                            .subscribe(new AbstractCommandShowError<Boolean>() {
                                @Override
                                public void onNext(Boolean aBoolean) {
                                    DialogManager.showInfoDialog("Успешно ", "Успешно добавлено");
                                }
                            });
                } else {
                    Repository.getResult(new UpdateStudentCommand(entityEditController.getEntity()))
                            .subscribe(new AbstractCommandShowError<Boolean>() {
                                @Override
                                public void onNext(Boolean aBoolean) {
                                    DialogManager.showInfoDialog("Успешно ", "Успешно Обновлено");
                                }
                            });
                }
            }
        } catch (Exception e) {

            DialogManager.showErrorDialog("Error", e.getMessage());
        }
    }

    private void clearField() {

        idLabel.setText("0");

        loginField.setText("");

        passwordField.setText("");

        if(typeEntities.size() > 0)
            roleApp.setValue(typeEntities.get(0));

        statusUser.setSelected(false);
        //statusUser.selectedProperty().setValue(user.getStatus());
        statusUser.selectedProperty().set(false);

        printStatusUser(statusUser.isSelected());
    }

    @Override
    public void setEntity(UserEntity entity) {
        this.entity = entity;
        if(entity != null){

            idLabel.setText(Integer.toString(entity.getIdUser()));

            loginField.setText(entity.getLogin());

            passwordField.setText(entity.getPassword());

            roleApp.setValue(entity.getRoleApplication());

            statusUser.setSelected(entity.getStatus() != 0);
            //statusUser.selectedProperty().setValue(user.getStatus());
            statusUser.selectedProperty().set(entity.getStatus() == 0);

            printStatusUser(statusUser.isSelected());
        } else {

            clearField();
        }
    }

    private void printStatusUser(Boolean newValue) {
        if(newValue){
            infoBlockStatusUser.setText("Пользователь заблокирован");
        } else {
            infoBlockStatusUser.setText("Пользователь не заблокирован");
        }
    }

    @Override
    public void initEntity() {

        if(isValid()) {

            if(entity == null){
                entity = new UserEntity();
            }
            entity.setLogin(loginField.getText());
            String pass = passwordField.getText();
            entity.setPassword(pass.equals(entity.getPassword()) ? entity.getPassword() : Crypto.sha256(passwordField.getText()));
            entity.setStatus(!statusUser.isSelected() ? 1 : 0);
            entity.setRoleApplication(roleApp.getValue());
        }

    }

    @Override
    public boolean isValid() {
        String errorMessage = "";
        if(loginField.getText() == null || loginField.getText().length() < 2 )
            errorMessage += "Логин не заполнен!\n";
        if(passwordField.getText() == null || passwordField.getText().length() < 2)
            errorMessage += "Пароль не заполнен!\n";
        if(passwordField.getText() != null && passwordField.getText().length() > 64)
            errorMessage += "Пароль слишком длинный. Введите пароль не больше 64 символов!\n";
        if(errorMessage.length() == 0)
            return true;
        else
            DialogManager.showErrorDialog("Ошибка ввода данных",errorMessage);
        return false;
    }

    @Override
    public UserEntity getEntity() {
        return entity;
    }

    @Override
    public void initialize() {

       /* rootPane.setBackground(Style.getBackground());
        dataPane.setBackground(Style.getBackground());
*/
        Repository.getResult(new RoleApplicationListCommand())
                .subscribe(new AbstractCommandShowError<RoleApplicationEntity[]>() {
                    @Override
                    public void onNext(RoleApplicationEntity[] roleApplicationEntities) {

                        typeEntities = ListUtil.parseListToObserv(typeEntities, Arrays.asList(roleApplicationEntities));
                        roleApp.setCellFactory(new Callback<ListView<RoleApplicationEntity>, ListCell<RoleApplicationEntity>>() {
                            @Override
                            public ListCell<RoleApplicationEntity> call(ListView<RoleApplicationEntity> param) {
                                final ListCell<RoleApplicationEntity> cell = new ListCell<RoleApplicationEntity>(){

                                    @Override
                                    protected void updateItem(RoleApplicationEntity t, boolean bln) {
                                        super.updateItem(t, bln);

                                        if(t != null){
                                            setText(t.getName());
                                        }else{
                                            setText(null);
                                        }
                                    }

                                };

                                return cell;
                            }
                        });
                        roleApp.setConverter(new StringConverter<RoleApplicationEntity>() {
                            @Override
                            public String toString(RoleApplicationEntity t) {
                                return t.getName();
                            }

                            @Override
                            public RoleApplicationEntity fromString(String string) {
                                return null;
                            }
                        });

                        roleApp.setItems(typeEntities);
                        roleApp.getSelectionModel().selectFirst();
                        roleApp.setValue(typeEntities.get(0));
                    }
                });
       }

    @Override
    public void displayIndicatorProgress() {
        dataPane.setVisible(false);
        progressIndicator.setVisible(true);
    }

    @Override
    public void closeIndicatorProgress() {
        dataPane.setVisible(true);
        progressIndicator.setVisible(false);
    }

    @Override
    public Pane getLayout() {
        return rootPane;
    }

    @Override
    public void setPrimaryStage(Stage primaryStage) {

    }
}
