package by.bsuir.ief.system.languagekourse.controller.form;

import by.bsuir.ief.system.languagekourse.controller.DialogManager;
import by.bsuir.ief.system.languagekourse.model.entity.LanguageEntity;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class LanguageFormController implements FormController<LanguageEntity> {

    @FXML
    private AnchorPane rootPane;
    @FXML
    private Label idLabel;

    @FXML
    private TextField name;

    @FXML
    private Pane dataPane;

    @FXML
    private ProgressIndicator progressIndicator;

    private LanguageEntity entity = null;
    @Override
    public void setEntity(LanguageEntity entity) {

        this.entity = entity;
        if(entity != null){

            idLabel.setText(String.valueOf(entity.getId()));
            name.setText(entity.getName());
        } else {

            idLabel.setText("0");
            name.setText("");
        }
    }

    @Override
    public void initEntity() {
        if(isValid()){

            if(entity == null)
                entity = new LanguageEntity();

            entity.setName(name.getText());
            entity.setId(Integer.parseInt(idLabel.getText()));
        }
    }

    @Override
    public boolean isValid() {

        String errorMessage = "";
        if(name.getText() == null || name.getText().length() < 1)
            errorMessage += "Названине  не заполнено!\n";

        if(errorMessage.length() == 0)
            return true;
        else
            DialogManager.showErrorDialog("Ошибка ввода данных",errorMessage);
        return false;
    }

    @Override
    public LanguageEntity getEntity() {

        return entity;
    }

    @Override
    public void initialize() {

       /* rootPane.setBackground(Style.getBackground());
        dataPane.setBackground(Style.getBackground());
*/
        idLabel.setText("0");
    }

    @Override
    public void displayIndicatorProgress() {
        dataPane.setVisible(false);
        progressIndicator.setVisible(true);
    }

    @Override
    public void closeIndicatorProgress() {
        dataPane.setVisible(true);
        progressIndicator.setVisible(false);
    }

    @Override
    public Pane getLayout() {
        return rootPane;
    }

    @Override
    public void setPrimaryStage(Stage primaryStage) {

    }
}
