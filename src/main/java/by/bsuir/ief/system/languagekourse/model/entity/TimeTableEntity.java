package by.bsuir.ief.system.languagekourse.model.entity;

import java.util.Date;
import java.util.Objects;

public class TimeTableEntity {
    private int id;
    private int room;
    private Date dateTime;
    private int idGroup;
    private String adress;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoom() {
        return room;
    }

    public void setRoom(int room) {
        this.room = room;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeTableEntity that = (TimeTableEntity) o;
        return id == that.id &&
                room == that.room &&
                idGroup == that.idGroup &&
                Objects.equals(dateTime, that.dateTime) &&
                Objects.equals(adress, that.adress);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, room, dateTime, idGroup, adress);
    }
}
