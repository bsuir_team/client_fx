package by.bsuir.ief.system.languagekourse.controller.factory;

import by.bsuir.ief.system.languagekourse.model.entity.LanguageEntity;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class LanguageCallbackCombox implements Callback<ListView<LanguageEntity>, ListCell<LanguageEntity>> {
    @Override
    public ListCell<LanguageEntity> call(ListView<LanguageEntity> param) {
        final ListCell<LanguageEntity> cell = new ListCell<LanguageEntity>() {

            @Override
            protected void updateItem(LanguageEntity t, boolean bln) {
                super.updateItem(t, bln);

                if (t != null) {
                    setText(t.getName());
                } else {
                    setText(null);
                }
            }
        };

        return cell;
    }
}
