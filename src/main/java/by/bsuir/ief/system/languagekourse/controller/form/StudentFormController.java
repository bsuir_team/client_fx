package by.bsuir.ief.system.languagekourse.controller.form;

import by.bsuir.ief.system.languagekourse.controller.DialogManager;
import by.bsuir.ief.system.languagekourse.controller.valid.TextFieldValid;
import by.bsuir.ief.system.languagekourse.controller.valid.ValidField;
import by.bsuir.ief.system.languagekourse.controller.widet.MaskField;
import by.bsuir.ief.system.languagekourse.model.entity.StudentEntity;
import by.bsuir.ief.system.languagekourse.util.DateUtil;
import by.bsuir.ief.system.languagekourse.util.Style;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.Date;

public class StudentFormController implements FormController<StudentEntity> {

    @FXML
    private Pane rootPane;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private GridPane contentView;
    @FXML
    private TextField surnameRequired;
    @FXML
    private TextField nameRequired;
    @FXML
    private TextField patronymicRequired;
    @FXML
    private DatePicker birthdayRequired;
    @FXML
    private MaskField mobilePhone;

    private StudentEntity entity = null;
    private ValidField<String> nameValid = new TextFieldValid();

    private boolean isEditable;

    @Override
    public void initialize() {

        //rootPane.setBackground(Style.getBackground());

        mobilePhone.setMask("+DDD (DD)-DDD-DD-DD");

    }

    @Override
    public void setEntity(StudentEntity entity) {

        this.entity = entity;
        if(entity != null){

            surnameRequired.setText(entity.getSurname());
            nameRequired.setText(entity.getName());
            patronymicRequired.setText(entity.getPatronymic());
            birthdayRequired.setValue(DateUtil.getLocalDate(entity.getBirthOfDate() == null ? new Date(): entity.getBirthOfDate()));

            if(entity.getNumber() != null && entity.getNumber().length() > 0)
                mobilePhone.setPlainText(entity.getNumber());

        } else {

            birthdayRequired.setValue(DateUtil.getLocalDate(new Date()));
        }
    }

    @Override
    public void initEntity() {
        if(isValid()){

            if(entity == null)
                entity = new StudentEntity();

            entity.setSurname(surnameRequired.getText());
            entity.setName(nameRequired.getText());
            entity.setPatronymic(patronymicRequired.getText());
            entity.setBirthOfDate(DateUtil.getDate(birthdayRequired.getValue()));

            if(mobilePhone.getPlainText().length() > 0 && !mobilePhone.getText().contains(Character.toString(MaskField.PLACEHOLDER_CHAR_DEFAULT))){

                entity.setNumber(mobilePhone.getPlainText());
            }
        }
    }

    @Override
    public boolean isValid() {

        String errorMessage = "";
        //Required field

        if (surnameRequired.getText().length() < 1){
            errorMessage += "Поле \"Фамилия\" обязательно к заполнению\n";
            surnameRequired.setStyle(Style.getErrorStyle());
        } else if(!nameValid.isValid(surnameRequired.getText())) {
            errorMessage += "Ошибка при вводе Фамилии\n";
            surnameRequired.setStyle(Style.getErrorStyle());
        } else {
            surnameRequired.setStyle(null);
        }

        if(nameRequired.getText().length() < 1) {
            errorMessage += "Поле \"Имя\" обязательно к заполнению\n";
            nameRequired.setStyle(Style.getErrorStyle());
        } else if(!nameValid.isValid(nameRequired.getText())) {
            errorMessage += "Ошибка ввода Имени\n";
            nameRequired.setStyle(Style.getErrorStyle());
        } else {
            nameRequired.setStyle(null);
        }

        if(patronymicRequired.getText().length() < 1) {
            errorMessage += "Поле \"Отчество\" обязательно к заполнению\n";
            patronymicRequired.setStyle(Style.getErrorStyle());
        } else if(!nameValid.isValid(patronymicRequired.getText())) {
            errorMessage += "Ошибка ввода Отчества\n";
            patronymicRequired.setStyle(Style.getErrorStyle());
        } else {
            patronymicRequired.setStyle(null);
        }

        if(birthdayRequired.getEditor().getText().length() < 1) {
            errorMessage += "Поле \"Дата рождения\" обязательно к заполнению\n";
            birthdayRequired.setStyle(Style.getErrorStyle());
        }  else {
            birthdayRequired.setStyle(null);
        }

        if(mobilePhone.getText().length() < 1) {

            errorMessage += "Поле \"мобильный телефон\" обязательно к заполнению\n";
            mobilePhone.setStyle(Style.getErrorStyle());
        } else if(mobilePhone.getPlainText().length() > 0 && mobilePhone.getText().contains(Character.toString(MaskField.PLACEHOLDER_CHAR_DEFAULT))){
            errorMessage += "Поле \"мобильный телефон\" не заполнено до конца!\n";
            mobilePhone.setStyle(Style.getErrorStyle());
        } else {
            mobilePhone.setStyle(null);
        }

        if(errorMessage.length() == 0)
            return true;
        else
            DialogManager.showErrorDialog("Ошибка ввода данных",errorMessage);
        return false;
    }

    /**
     * Вызывать только после метода {@link #initEntity()}
     * @return
     */
    @Override
    public StudentEntity getEntity() {

        return entity;
    }


    @Override
    public void displayIndicatorProgress() {
        contentView.setVisible(false);
        progressIndicator.setVisible(true);
    }

    @Override
    public void closeIndicatorProgress() {
        contentView.setVisible(true);
        progressIndicator.setVisible(false);
    }

    @Override
    public Pane getLayout() {
        return rootPane;
    }

    @Override
    public void setPrimaryStage(Stage primaryStage) {

    }

}
