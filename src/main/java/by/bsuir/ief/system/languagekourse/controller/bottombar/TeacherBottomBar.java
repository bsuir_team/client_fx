package by.bsuir.ief.system.languagekourse.controller.bottombar;

import by.bsuir.ief.system.languagekourse.controller.CreateEditDeleteBottomController;
import by.bsuir.ief.system.languagekourse.controller.DialogManager;
import by.bsuir.ief.system.languagekourse.controller.form.FormController;
import by.bsuir.ief.system.languagekourse.controller.table.TableController;
import by.bsuir.ief.system.languagekourse.model.entity.TeacherEntity;
import by.bsuir.ief.system.languagekourse.model.storage.Repository;
import by.bsuir.ief.system.languagekourse.model.storage.command.AbstractCommandShowError;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.teacher.AddTeacherCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.teacher.DeleteTeacherCommand;
import by.bsuir.ief.system.languagekourse.model.storage.command.koursecommand.teacher.UpdateTeacherCommand;

public class TeacherBottomBar implements CreateEditDeleteBottomController.CallbackBottomBarManipulation {

    private final TableController<TeacherEntity> tableController;
    private final FormController<TeacherEntity> formController;

    public TeacherBottomBar(TableController<TeacherEntity> tableController,
                            FormController<TeacherEntity> formController) {
        this.tableController = tableController;
        this.formController = formController;
    }

    @Override
    public void onCreate() {

        if (formController.isValid()) {
            formController.initEntity();
            Repository.getResult(new AddTeacherCommand(formController.getEntity()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                @Override
                public void onNext(Boolean aBoolean) {
                    DialogManager.showInfoDialog("Операция завершена успешно!", "Создание завершено успешно");
                    Repository.getsTeacher(tableController);
                }
            });
        }
    }

    @Override
    public void onEdit() {
        if (formController.isValid()) {
            formController.initEntity();

            Repository.getResult(new UpdateTeacherCommand(formController.getEntity()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                @Override
                public void onNext(Boolean aBoolean) {

                    DialogManager.showInfoDialog("Операция завершена успешно!", "Редактирование завершено успешно");
                    Repository.getsTeacher(tableController);
                }
            });
        }
    }

    @Override
    public void onDelete() {

        TeacherEntity entity = formController.getEntity();
        if(entity != null) {
            Repository.getResult(new DeleteTeacherCommand(formController.getEntity().getId()))
                    .subscribe(new AbstractCommandShowError<Boolean>() {
                        @Override
                        public void onNext(Boolean aBoolean) {
                            DialogManager.showInfoDialog("Операция завершена успешно!", "Удаление завершено успешно");
                            Repository.getsTeacher(tableController);
                        }
                    });
        } else {

            DialogManager.showWarningDialog("Ошибка!", " Не выбран элемент для удаления!!");
        }

    }
}
