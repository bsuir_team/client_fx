package connection.client.responsebody;

import connection.enumer.MediaType;

import java.io.BufferedReader;
import java.io.IOException;

public class StringResponseBody extends ResponseBody {

    private String string;

    StringResponseBody(long mHeaderContentLength) {
        super(mHeaderContentLength);
    }

    @Override
    public MediaType contentType() {
        return MediaType.STRING;
    }

    @Override
    protected void readTo(BufferedReader inputStream) throws IOException {

        string = inputStream.readLine();
    }

    @Override
    public long contentLength() {
        return string.length();
    }

    @Override
    public byte[] bytes() {
        return string.getBytes();
    }
}
