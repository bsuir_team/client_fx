package connection.client.responsebody;

import connection.enumer.MediaType;

public class JsonResponseBody extends StringResponseBody {

    protected JsonResponseBody(long mHeaderContentLength) {
        super(mHeaderContentLength);
    }

    @Override
    public MediaType contentType() {
        return MediaType.JSON;
    }
}
