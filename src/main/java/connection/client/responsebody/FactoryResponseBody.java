package connection.client.responsebody;

import com.sun.istack.internal.NotNull;
import connection.enumer.MediaType;

public class FactoryResponseBody {


    public static ResponseBody create(@NotNull final MediaType mediaType, final long contentLength){

        ResponseBody responseBody;

        switch (mediaType){
            case NO_CONTENT:{

                responseBody = new NoContentResponceBody(contentLength);
            } break;
            case FILE:{

                throw new RuntimeException("No impl");
            }
            case JSON:{

                responseBody = new JsonResponseBody(contentLength);
            } break;
            case STRING:{

                responseBody = new StringResponseBody(contentLength);
            }break;
            default:
                throw new RuntimeException("No impl");
        }

        return responseBody;
    }
}
