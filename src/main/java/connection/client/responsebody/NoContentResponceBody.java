package connection.client.responsebody;

import connection.enumer.MediaType;

import java.io.BufferedReader;

public class NoContentResponceBody extends ResponseBody {

    NoContentResponceBody(long mHeaderContentLength) {
        super(mHeaderContentLength);
    }

    @Override
    public MediaType contentType() {
        return MediaType.NO_CONTENT;
    }

    @Override
    protected void readTo(BufferedReader inputStream) {

    }

    @Override
    public long contentLength() {
        return 0;
    }

    @Override
    public byte[] bytes() {
        return new byte[0];
    }

}
