package connection.client.requestbody;

import com.sun.istack.internal.NotNull;
import connection.enumer.MediaType;

import java.io.IOException;
import java.io.PrintStream;

public abstract class RequestBody {

    /** Writes the content of this request to {@code sink}. */
    abstract void writeTo(@NotNull final PrintStream outputStream) throws IOException;

    public abstract MediaType contentType();

    public abstract long contentLength();

    public void execute(@NotNull final PrintStream outputStream) throws IOException {

        writeTo(outputStream);
    }
}
