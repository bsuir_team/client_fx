package connection.client.requestbody;

import com.sun.istack.internal.NotNull;
import connection.enumer.MediaType;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class StringRequestBody extends RequestBody {

    private final String senderString;

    public StringRequestBody(@NotNull final String senderString) {
        this.senderString = senderString;
    }

    @Override
    void writeTo(PrintStream outputStream) throws IOException {

        //для записи потока байт
        PrintStream sout = new PrintStream(new BufferedOutputStream(outputStream), true);

        sout.println(senderString);
    }

    @Override
    public MediaType contentType() {
        return MediaType.STRING;
    }

    @Override
    public long contentLength() {
        return senderString.length();
    }
}
