package connection.client.requestbody;

import connection.enumer.MediaType;

import java.io.IOException;
import java.io.PrintStream;

final public class NoContentRequestBody  extends RequestBody {


    @Override
    void writeTo(PrintStream outputStream) throws IOException {
        // TODO: stub
    }

    @Override
    public MediaType contentType() {
        return MediaType.NO_CONTENT;
    }

    @Override
    public long contentLength() {
        return 0;
    }
}
