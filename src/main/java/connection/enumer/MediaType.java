package connection.enumer;

public enum MediaType {
    FILE,
    NO_CONTENT,
    STRING,
    JSON;
}
